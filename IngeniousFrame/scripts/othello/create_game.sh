#!/bin/bash

# creates a new Othello game with the default configuration
java -jar ../../build/libs/IngeniousFrame-all-0.0.2.jar create \
-config "Othello.json" -game "OthelloReferee" -lobby "mylobby"
