package za.co.percipio.mpl;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import za.co.percipio.mpl.connection.ConnectionHandler;
import za.co.percipio.mpl.listener.ConnectionEventListener;
import za.co.percipio.mpl.listener.ServerConnectionListener;

public class MessageServer implements Runnable {
    private String                   hostname;
    private int                      serverPort;
    private ServerSocketChannel      serverChannel;
    private Selector                 selector;
    private ServerConnectionListener serverConnectionListener;
    private Thread                   thread;
    private ConnectionHandler[]      handlers;
    private int                      nextHandler;

    private InetAddress connectedAddress;

    private boolean connectionErrorOccured;
    private boolean isConnected;

    public MessageServer(String hostname, int port, ServerConnectionListener serverListener,
            ConnectionEventListener clientConnectionListener, int handlerCount) {
        if (handlerCount < 0) throw new RuntimeException("Handler Count cannot be negative");
        this.hostname = hostname;
        this.serverPort = port;
        serverConnectionListener = serverListener;
        handlers = new ConnectionHandler[handlerCount];
        for (int i = 0; i < handlerCount; i++) {
            handlers[i] = new ConnectionHandler(clientConnectionListener);
            handlers[i].start();
        }
        thread = new Thread(this);
    }

    public void startAsync() {
        thread.start();
    }

    public void startOnCurrentThread() throws IOException {
        if (!isConnected) {
            initializeConnectionInternal();
        }
    }

    private synchronized void setConnected(boolean connected) {
        this.isConnected = connected;
    }

    private synchronized boolean isConnected() {
        return this.isConnected;
    }

    private void initializeConnection() {
        InetSocketAddress address;
        try {
            initializeConnectionInternal();
        } catch (IOException e) {
            Log.debug(this.getClass().getName(),"Connection error occured while connecting to the service", e);
            internalOnErrorOccured();
        }
    }

    private void initializeConnectionInternal() throws IOException {
        InetSocketAddress address;
        address = new InetSocketAddress(hostname, serverPort);
        serverChannel = ServerSocketChannel.open();
        serverChannel.socket().setReuseAddress(true);
        serverChannel.socket().bind(address);
        serverChannel.configureBlocking(false);
        selector = Selector.open();
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        connectedAddress = serverChannel.socket().getInetAddress();
        setConnected(true);
        serverConnectionListener.onServerConnectionEstablished(this);
    }

    @Override public void run() {
        if (!isConnected()) {
            /* might have started on the creating thread */
            initializeConnection();
        }
        if (connectionErrorOccured) return;
        for (; ; ) {
            try {
                SocketChannel clientSocket;
                selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iter = keys.iterator();
                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    iter.remove();
                    if (key.isAcceptable()) {
                        clientSocket = serverChannel.accept();
                        getNextHandler().manageSocketChannel(clientSocket);
                    }
                }
            } catch (IOException e) {
                Log.debug(this.getClass().getName(),"[Server] ConnectionError during run.", e);
                internalOnErrorOccured();
            }
        }
    }

    private ConnectionHandler getNextHandler() {
        return handlers[(nextHandler++) % handlers.length];
    }

    public void stop() {
        disconnectInternal();
        /* Notify the thread of having stopped */
    }

    private void disconnectInternal() {
        if (isConnected()) {
            setConnected(false);
            serverConnectionListener.onServerDisconnection(this);
        }
    }

    private void internalOnErrorOccured() {
        connectionErrorOccured = true;
        serverConnectionListener.onServerError(this);
        disconnectInternal();
    }

    public InetAddress address() {
        InetAddress address = connectedAddress;
        if (!isConnected()) {
            throw new RuntimeException("Server is not connected yet!");
        }
        return address;
    }

}
