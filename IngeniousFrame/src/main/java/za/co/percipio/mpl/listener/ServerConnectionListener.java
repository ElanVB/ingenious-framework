package za.co.percipio.mpl.listener;

import za.co.percipio.mpl.MessageServer;

public interface ServerConnectionListener {
    public void onServerConnectionEstablished(MessageServer server);
    public void onServerDisconnection(MessageServer server);
    public void onServerError(MessageServer server);
}
