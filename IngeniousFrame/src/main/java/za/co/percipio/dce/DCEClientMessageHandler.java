package za.co.percipio.dce;

import com.google.common.eventbus.Subscribe;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.network.lobby.GameServer;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGameTerminateMessage;
import za.co.percipio.dce.message.ClientConfigurationMessage;
import za.co.percipio.dce.message.DCEFinishedMessage;
import za.co.percipio.dce.message.DCEPlayerMessage;
import za.co.percipio.dce.message.DCEResultMessage;
import za.co.percipio.mpl.Message;
import za.co.percipio.mpl.connection.Connection;
import za.co.percipio.mpl.listener.ConnectionEventListener;

/**
 * Created by Chris Coetzee on 2016/07/28.
 */
public class DCEClientMessageHandler implements ConnectionEventListener {
    private String     hostname;
    private int        port;
    private String     lobbyName;
    private String     gameName;
    private Connection serverConnection;

    private GameServer gameServer;

    private ArrayList<String> playerNames = new ArrayList<>();

    public DCEClientMessageHandler(GameServer gameServer, String hostname, int port, String gameName,
            String lobbyName) {
        this.hostname = hostname;
        this.port = port;
        this.lobbyName = lobbyName;
        this.gameName = gameName;

        this.gameServer = gameServer; // need to disconnect this at the end
    }

    @Override public void onMessageReceived(Connection connection, Message message) {
        /* FIXME remove hardcoded string */
        if (message instanceof DCEPlayerMessage) {
            DCEPlayerMessage msg = (DCEPlayerMessage) message;
            connection.queueMessage(new ClientConfigurationMessage(hostname, port, gameName,
                                                                   lobbyName, msg.getUsername()));
            playerNames.add(msg.getUsername());
        } else if (message instanceof DCEFinishedMessage) {
            connection.disconnect();
            /* TODO remove this after fixing #83 */
            System.exit(0);
        }
    }

    @Override public void onSocketConnection(Connection connection) {
        this.serverConnection = connection;

    }

    @Override public void onSocketDisconnection(Connection connection) {

    }

    @Override public void onConnectionError(Connection c) {

    }

    @Override public void onIncomingMessageParsingError(Connection connection, Throwable t) {

    }

    /* Eventbus listener */
    @Subscribe private void onGameTerminated(BMGameTerminateMessage a) {

        ArrayList<String> scores = new ArrayList<String>();
        for (double d : a.getWinningScore()) {
            scores.add(Double.toString(d));
        }

        DCEResultMessage msg = new DCEResultMessage(playerNames, scores);
        a.getWinningScore();
        serverConnection.queueMessage(msg);

        /* stop the game server */
        /* TODO see #83 */
        gameServer.close();
    }

}
