package za.ac.sun.cs.ingenious.games.mnk.engines;

import com.esotericsoftware.minlog.Log;

import java.util.Map;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.cfr.Helper;
import za.ac.sun.cs.ingenious.search.cfr.OOS;

public class MNKOOSEngine extends MNKEngine {
	
	OOS<MNKState> oos;
	private final static int numIterations = 100000;

	public MNKOOSEngine(EngineToServerConnection toServer) {
		super(toServer);
		oos = new OOS<MNKState>(
				logic, logic,
				new MNKFinalEvaluator(),
				logic,
				0.4);
	}

	@Override
	public String engineName() {
		return "MNKOOSEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		oos.iterate(board, numIterations, 1.0);

		Map<Action, Double> strat = Helper.getAverageStrategyProfile(
				oos.getCumulativeStrategy(logic.observeState(board, board.nextMovePlayerID)));
		Log.info("Average profile at current state:");
		Log.info(strat);

		return new PlayActionMessage(Helper.sampleFromStrategy(strat));
	}
	
	public static void main(String[] args) {
		MNKOOSEngine e = new MNKOOSEngine(new EngineToServerConnection(null, null, null, 0));
		e.board = new MNKState(3, 3, 3, true, 2);
		PlayActionMessage m = e.receiveGenActionMessage(null);
		Log.info("Chosen action:");
		Log.info(m.getAction());
	}

}
