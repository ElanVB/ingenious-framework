package za.ac.sun.cs.ingenious.games.tictactoe;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

/**
 * Sample class for a game logic. For descriptions of what the methods do, see the
 * {@link GameLogic} interface.
 * 
 * @author Stephan Tietz
 * @author Michael Krause
 */
public class TTTLogic implements TurnBasedGameLogic<TurnBasedSquareBoard> {

	@Override
	public boolean validMove(TurnBasedSquareBoard fromState, Move move) {
		XYAction xyMove = (XYAction) move;
		return fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY())] == 0;
	}

	@Override
	public boolean makeMove(TurnBasedSquareBoard fromState, Move move) {
		XYAction xyMove = (XYAction) move;
		fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY())] = (byte)(xyMove.getPlayerID()+1);
		nextTurn(fromState, move);
		return true;
	}

	@Override
	public void undoMove(TurnBasedSquareBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Action> generateActions(TurnBasedSquareBoard fromState, int forPlayerID) {
		ArrayList<Action> actions = new ArrayList<>();
		for(byte i = 0; i < fromState.board.length; i++){
			if(fromState.board[i]==0){
				actions.add(new XYAction((short)(i%3), (short)(i/3), forPlayerID));
			}
		}
		return actions;
	}

	@Override
	public boolean isTerminal(TurnBasedSquareBoard state) {
		for(int i = 0; i < 9; i= i+3){
			if(state.board[i+0] != 0 &&  state.board[i+0] == state.board[i+1] && state.board[i+1] == state.board[i+2]) {
				return true;
			}
		}
		for(int i = 0; i < 3; i++){
			if(state.board[i+0] != 0 && state.board[i+0] == state.board[i+3] && state.board[i+3] == state.board[i+6]) {
				return true;
			}
		}
		if(state.board[0] != 0 && state.board[0] == state.board[4] && state.board[4] == state.board[8]) {
			return true;
		}
		if(state.board[2] != 0 && state.board[2] == state.board[4] && state.board[4] == state.board[6]) {
			return true;
		}
		
		return full(state);
	}

	/**
	 * @param state Some game state
	 * @return True if the board in the given game state is filled completely
	 */
	public boolean full(TurnBasedSquareBoard state) {
		for(byte i = 0; i < state.board.length; i++){
			if(state.board[i]==0) {
				return false;
			}
		}
		return true;
	}
}
