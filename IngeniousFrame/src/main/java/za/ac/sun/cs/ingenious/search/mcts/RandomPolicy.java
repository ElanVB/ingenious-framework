package za.ac.sun.cs.ingenious.search.mcts;

import com.esotericsoftware.minlog.Log;

import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Standard implementation of a PlayoutPolicy that just plays the game by letting each player
 * pick a random action until a terminal state is reached. No domain knowledge needed.
 * 
 * @author steve
 * @author Michael Krause
 * @param <S> The type of TurnBasedGameState to operate the playout on
 */
public class RandomPolicy<S extends TurnBasedGameState> implements PlayoutPolicy<S> {

	private GameLogic<S> logic;
	private GameFinalEvaluator<S> eval;
	private ActionSensor<S> obs;
	private boolean normalize;
	
	/**
	 * @param logic The game logic to play the game with.
	 * @param eval An evaluator for terminal states of the game.
	 * @param obs Object to process actions made by the players during playouts. Actions are
	 *            observed from the point of view of the environment player.
	 * @param normalizeScores True, if the scores returned by eval are not normalized, i.e: not
	 *            every score is between 0 and 1 where 0 denotes failure and 1 complete
	 *            success.
	 */
	public RandomPolicy(GameLogic<S> logic, GameFinalEvaluator<S> eval, ActionSensor<S> obs, boolean normalizeScores) {
		this.logic = logic;
		this.eval = eval;
		this.normalize = normalizeScores;
		this.obs = obs;
	}
	
	@Override
	public PlayoutResult playout(S origState) {
		@SuppressWarnings("unchecked")
		S state = (S) origState.deepCopy();
		
		int numSteps = 0;
		while(!logic.isTerminal(state)){
			numSteps++;
			// First generate all actions ...
			List<Action> actions = logic.generateActions(state, state.nextMovePlayerID);
			
			if (!actions.isEmpty()) { // ... if any actions are possible ...
				// ... apply a random one.
				Action randomAction = actions.get((int) (Math.random() * actions.size()));
				logic.makeMove(state, obs.fromPointOfView(randomAction, state, -1));
			} else {
				Log.error("RandomPolicy", "Error during playout: state is not terminal but no actions possible!");
				break;
			}
		}
		
		double[] scores = eval.getScore(state);
		if (normalize) {
			double max = Double.NEGATIVE_INFINITY;
			for (double d : scores) {
				if (d>max) {
					max = d;
				}
			}
			for (int i=0; i<scores.length; i++) {
				scores[i] = scores[i]/max;
			}
		}

		return new PlayoutResult(scores, numSteps);
	}

}
