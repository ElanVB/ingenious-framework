package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import java.awt.Point;

public class Bomb {

    public static final int DEFAULT_MAX_BOMB_TIMER = 10;

    private static int maxBombTimer = DEFAULT_MAX_BOMB_TIMER;
	private Point position;
	private short timer;
	private short radius;
	private int id;
	
	public Bomb(Point position, short timer, short radius, int id) {
        if (timer < 1 || timer > maxBombTimer) throw new IllegalArgumentException("Invalid bomb timer: "+timer);
        if (id < 0) throw new IllegalArgumentException("Negative player id for bomb invalid");
		this.position = position;
		this.timer = timer;
		this.radius = radius;
        this.id = id;
	}
	
    /**
     * Adjust the maximum bomb timer that a player can have.
     *
     * @param t New maximum bomb timer value.  Must be positive.
     */
    protected void setMaxBombTimer(int t) {
        if (t > 0) {
            maxBombTimer = t;
        } else {
            throw new IllegalArgumentException("Max bomb timer must be positive");
        }
    }

	public Point getPosition() {
		return position;
	}
	
	public short getTimer() {
		return timer;
	}

	public void decreaseTimer() {
        if (timer > 0) {
    		timer--;
        } else {
            throw new IllegalStateException("Attempt to decrease non-positive bomb timer.");
        }
	}
	
	public short getRadius() {
		return radius;
	}
	
	public int getId() {
		return id;
	}

	protected void setId(int id) {
        if (id < 0) throw new IllegalArgumentException("Negative player id for bomb invalid");
		this.id = id;
	}

	protected void trigger() {
		timer = 1;
	}

    public boolean similar(Bomb b) {
        if (b == null) return false;
        if (id != b.id || radius != b.radius || timer != b.timer) return false;
        if (position == null) {
            if (b.position != null) return false;
        } else {
            if (!position.equals(b.position)) return false;
        }
        return true;
    }
}
