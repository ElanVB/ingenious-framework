package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import com.esotericsoftware.minlog.Log;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.IdleAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.PlaceBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.TriggerBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.NewUpgradesMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NextRoundMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;


public class BMLogic implements GameLogic<BMBoard> {

    /**
     * Determines the maximum amount of rounds player before the game will terminate.
     */
    protected int maxRounds = 200;

    /* Set default parameters for scoring */
    protected int pointsForUpgrade = 50;
    protected int pointsForKill = 900;
    protected int pointsForSuicide = -200;
    protected int pointsForCradleDestroyed = 20;
    protected int pointsForDeath = -200;
    protected boolean visibleUpgrades;
    // TODO: provide mechanism for setting these things - see issue 199

    /**                          
     * Parameter controlling likelihood of upgrades.  This proportion of cradles
     * will hide or be converted into bomb upgrades, and similarly for blast upgrades
     */ 
    protected final static double bombUpgradeFactor = 0.1;
    protected final static double blastUpgradeFactor = 0.1;

	public static final BMLogic defaultBMLogic = new BMLogic(false);

	public BMLogic(boolean visibleUpgrades) {
        this.visibleUpgrades = visibleUpgrades;
    }

	@Override
	public boolean validMove(BMBoard fromState, Move move) {
		//idling or triggering bombs is always valid
		if(move instanceof IdleAction || move instanceof TriggerBombAction){
			return true; 
		}
		if (move instanceof PlaceBombAction) {
			PlaceBombAction m = (PlaceBombAction) move;
			Player p = fromState.getPlayers()[m.getPlayerID()];
			return p.isAlive() && p.getNofBombs() > 0 && !fromState.isBomb(p.getPosition()); // TODO bombs shouldn't be detonated twice - issue 177
		} else if (move instanceof CompassDirectionAction) {
			CompassDirectionAction m = (CompassDirectionAction) move;
            if (!m.getDir().isCardinal()) {
                return false;
            }
			Player p = fromState.getPlayers()[m.getPlayerID()];
			return fromState.isPassable(fromState.getPos(p.getPosition(), m.getDir()));
		}
		return false;
	}

	/**
	 * Applies the provided message to the given state.
     * There are four different messages in Bomberman, each of them is a child of {@link PlayActionMessage}.
     * 1) {@link UpdateBombsMessage}: Sent at the beginning of a new round, decrease all bombtimers, explode bombs that have timer == 0
     * 2) {@link NewUpgradesMessage}: Sent if exlpoded bombs revealed any new upgrades.
     * 3) {@link PlayedMoveMessage}: Contains an actual move by a player.
     * 4) {@link NextRoundMessage}: Sent after the round is finished. When NextRoundMessage is received all explosion and exploded objects must be removed from the map.
	 *
	 * The first two and last of these types are distributed by the referee to ensure correct game flow between player moves.
	 *
	 * @param a Message to apply
	 * @param fromState Game state to apply message to
	 *
	 * @return true for supported messages, false for an unexpected type of PlayMoveMessage.
	 */
	public boolean applyPlayedMoveMessage(PlayedMoveMessage a, BMBoard fromState) {
        // TODO: Consider using the Visitor pattern to make this more generic, and refactor into higher classes - see issue 174
        if (a instanceof UpdateBombsMessage){
			UpdateBombsMessage m = (UpdateBombsMessage) a;
            updateBombs(m.getPlayerID(), m.generateUpgrades(), fromState);
        } else if(a instanceof NewUpgradesMessage){
            NewUpgradesMessage upg = (NewUpgradesMessage) a;
            addUpgrades(upg.getBombUpgrades(), upg.getBlastUpgrades(), fromState);
        } else if(a instanceof NextRoundMessage){
            nextRound(fromState);
		} else if (a instanceof PlayedMoveMessage) {
			makeMove(fromState, a.getMove());
        } else {
			return false; // Unsupported PlayMoveMessage
        }
		return true;
	}

	/**
	 * Applies a player's move to the given board.  Does NOT check if the move is valid in the given position.
	 * @param fromState Game state to apply move to.
	 * @param move Move to apply.
	 *
	 * @return true for recognized move types; false otherwise.
	 */
	@Override
	public boolean makeMove(BMBoard fromState, Move move) {
		if (move instanceof PlaceBombAction) {
			PlaceBombAction m = (PlaceBombAction) move;
			placeBomb(m.getPlayerID(), fromState);
		} else if (move instanceof CompassDirectionAction) {
			CompassDirectionAction m = (CompassDirectionAction) move;
			movePlayer(m.getPlayerID(), m.getDir(), fromState);
		} else if (move instanceof TriggerBombAction) {
			triggerBomb(((TriggerBombAction)move).getPlayerID(), fromState);
		} else if (!(move instanceof IdleAction)) {
			return false;
		}
		nextPlayer(fromState);
		return true;
	}

	public void nextPlayer(BMBoard fromState) {
		if (fromState.isAlternating()) {
			int lastPlayer = fromState.alternatingMovingPlayer;
			do {
			fromState.alternatingMovingPlayer = (fromState.alternatingMovingPlayer+1) % fromState.getNumPlayers();
			} while (fromState.alternatingMovingPlayer != lastPlayer && !fromState.getPlayer(fromState.alternatingMovingPlayer).isAlive());
		}
	}

	@Override
	public void undoMove(BMBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Action> generateActions(BMBoard fromState, int forPlayerID) {
		Player currentPlayer = fromState.getPlayers()[forPlayerID];
		List<Action> actions = new ArrayList<>();
		if (!currentPlayer.isAlive()) {
            Log.error("Generating moves for dead player with ID " + forPlayerID);
			return actions;
		}
		actions.add(new IdleAction());
		actions.add(new TriggerBombAction(forPlayerID));
		if(validMove(fromState, new PlaceBombAction(forPlayerID))) {
			actions.add(new PlaceBombAction(forPlayerID));
		}
		for (CompassDirection c: CompassDirection.cardinalDirections()) {
			Action m = new CompassDirectionAction(forPlayerID, c);
			if (validMove(fromState, m)) {
				actions.add(m);
			}
		}
		return actions;
	}
	
	@Override
	public boolean isTerminal(BMBoard state) {
		int playerAlive = 0;
		for (Player p : state.getPlayers()){
			if (p.isAlive()) {
				playerAlive++;
			}
		}
		return state.getRound() > maxRounds || playerAlive < 2;
	}

    /**
     * This method contains all bomb and exploding logic. 
     * 1) It increases the round counter (as update bombs always happens as the first thing of each round)
     * 2) It removes all explosions of the previous round
     * 3) It decreases all bomb timers and explodes bombs with timer 0.
     *
     * @param playerID ID of player whose bombs should be updated.  If a playerID of -1 is passed, all player's bombs are updated.
     * This does NOT correspond to updating each player's bombs in turn, because the order of detonations may change.
     */
    protected void updateBombs(int playerID, boolean genUpgrades, BMBoard fromState) {
        for(Node[] line : fromState.getNodes()){
            for(Node n : line){
                n.markExploding(false);
            }
        }
        ArrayList<Bomb> explodedBombs = new ArrayList<>();
        for (Bomb b : fromState.getBombs()) {
            if(!explodedBombs.contains(b) && (playerID == -1 || b.getId() == playerID)){
                b.decreaseTimer();
                if (b.getTimer() == 0) {
                    detonateBomb(b, explodedBombs, genUpgrades, fromState);
                }
            }
        }
        fromState.getBombs().removeAll(explodedBombs);
    }

    /**
     * Update discovered upgrades on the map.
     *
     * @param bombs
     * @param blasts
     */
	protected void addUpgrades(Iterable<Point> bombs, Iterable<Point> blasts, BMBoard fromState){
        if (visibleUpgrades)
		 {
			return; // No new upgrades added, since all visible
		}
		Node[][] nodes = fromState.getNodes();
        for(Point p : bombs){
            fromState.addVisibleBombUpgrade(nodes[p.y][p.x]);
        }
        for(Point p : blasts){
            fromState.addVisibleBlastUpgrade(nodes[p.y][p.x]);
        }
    }

    /**
     * Increment the round number of the state.
     */
    protected void nextRound(BMBoard fromState) {
        fromState.incrementRound();
    }

    public double scoreRelative(int id, BMBoard fromState){
        int totalScore = 0;
        for(int i = 0; i < fromState.getPlayers().length; i++){
            totalScore += fromState.score(i);
        }                
        return (double) fromState.score(id)/ (double) totalScore;
    }

    /**            
     * Identify a bomb of the player with given id with lowest timer, and trigger it.
     */            
    protected void triggerBomb(int id, BMBoard fromState){
        Bomb lastBomb = null;
        int maxTrigger = Integer.MAX_VALUE;
        for(Bomb b : fromState.getBombs()){
            if(b.getId() == id){
                if(b.getTimer() <= maxTrigger){
                    lastBomb = b;
                    maxTrigger = b.getTimer();
                }
            }
        }
        if(lastBomb != null){
            lastBomb.trigger();
        }
    }

    /**         
     * Place a bomb for the player with the given ID at his current location.
     *
     * @param id ID of player to place bomb.
     */         
    protected void placeBomb(int id, BMBoard fromState) {
        Player p = fromState.getPlayers()[id];
        fromState.placeBomb(new Point(p.getPosition().x, p.getPosition().y), p.getBombTimer(), p.getBlastRadius(), id);
    }  

    /** 
     * Moves the player in the given direction. If it moves onto an upgrade the player automatically collects it. 
     *  
     * @param id Player ID            
     * @param dir Direction of movement 
     */ 
    protected void movePlayer(int id, CompassDirection dir, BMBoard fromState) {
        Player p = fromState.getPlayers()[id];       
        Node oldNode = fromState.getNodes()[p.getPosition().y][p.getPosition().x];                                          
        oldNode.removePlayer();
        p.setPosition(BMBoard.getPos(p.getPosition(), dir));                                                         
        Node n = fromState.getNodes()[p.getPosition().y][p.getPosition().x];                                                
        n.setPlayer(p);
        if (n.hasBlastUpgrade()) {    
            p.incBlastRadius();       
            fromState.removeVisibleBlastUpgrade(n);
            fromState.adjustScore(p, pointsForUpgrade);                                                          
            fromState.notifyObservers("Player "+(char)('A'+p.getId())+" blast-upgrade.");                              
        }                             
        if (n.hasBombUpgrade()) {     
            p.incMaxBombs();          
            fromState.removeVisibleBombUpgrade(n);
            fromState.adjustScore(p, pointsForUpgrade);                                                          
            fromState.notifyObservers("Player "+(char)('A'+p.getId())+" bomb-upgrade.");                               
        }                             
    } 

	/**
	 * Detonates the specified bomb. If another bomb is within explosion radius of this bomb, it will detonate as well.
	 * @param b The bomb to detonate
	 * @param explodedBombs The array containing all exploded bombs. If another bomb is in the explosion radius of this bomb, it will be added to the array, to prevent it from exploding twice.
	 */
	protected void detonateBomb(Bomb b, ArrayList<Bomb> explodedBombs, boolean genUpgrades, BMBoard fromState) {
        Node[][] nodes = fromState.getNodes();
		nodes[b.getPosition().y][b.getPosition().x].removeBomb();
		explodedBombs.add(b);
		if(b.getId() != -1) {
            fromState.getPlayers()[b.getId()].incNofBombs();
        } else {
            // In the past, some bombs did not have an owner because of assumptions made during initialization
            // This should no longer be the case!
            throw new IllegalArgumentException("Bomb with invalid owner.");
        }
		for (CompassDirection i: CompassDirection.cardinalDirections()) {
			Point pos = b.getPosition();
			for(int j = 0; j < 1 + b.getRadius(); j++){
				nodes[pos.y][pos.x].markExploding(true);
				Player p = fromState.getPlayerAt(pos);
				if (p != null && p.isAlive()) {
					killPlayer(p, b, fromState);
					break;
				} else if (fromState.isWall(pos)) {
					break;
				} else if (nodes[pos.y][pos.x].hasBomb()){
					nodes[pos.y][pos.x].getBomb().setId(b.getId());
                    //TODO if this bomb also has a timer of 0, it should be its own detonation center, rather than part of a detonation chain.
					detonateBomb(nodes[pos.y][pos.x].getBomb(), explodedBombs, genUpgrades, fromState);
				} else if (fromState.isCradle(pos)) {
					if(b.getId() != -1){
						fromState.adjustScore(fromState.getPlayers()[b.getId()], pointsForCradleDestroyed, false);
					}
                    nodes[pos.y][pos.x].removeCradle();
                    if (!visibleUpgrades && genUpgrades) {
                        fromState.placeUpgrade(nodes[pos.y][pos.x], bombUpgradeFactor, blastUpgradeFactor);
                    }
					break;
				} 
				pos = BMBoard.getPos(pos, i);
			}
		}
	}

	protected void killPlayer(Player p, Bomb b, BMBoard fromState){
		Point pos = p.getPosition();
		p.setAlive(false);
		fromState.getNodes()[pos.y][pos.x].removePlayer();
		if(b.getId() != -1){ 
			if (p.getId() != b.getId()) {
				fromState.adjustScore(fromState.getPlayers()[b.getId()], pointsForKill);
				fromState.notifyObservers("Player "+ (char) ('A'+b.getId()) + " killed " + (char) ('A'+p.getId()) + ".");
			} else {
				fromState.adjustScore(fromState.getPlayers()[b.getId()], pointsForSuicide);
				fromState.notifyObservers("Player " + (char) ('A'+b.getId()) + " suicide.");
			}
        } else {
            // In the past, some bombs did not have a valid owner because of assumptions made during initialization
            // This should no longer be the case!
            throw new IllegalArgumentException("Bomb with invalid owner.");
        }
	}

	@Override
	public Set<Integer> getCurrentPlayersToAct(BMBoard fromState) {
        // TODO: game state should encode whose turn it is to play - see issue 196
		Set<Integer> ret = new HashSet<Integer>();
		if (fromState.isAlternating()) {
			ret.add(fromState.alternatingMovingPlayer);
		} else {
			for (int i = 0; i < fromState.getNumPlayers(); i++) {
				if (fromState.getPlayer(i).isAlive()) {
					ret.add(i);
				}
			}
		}
		return ret;
	}
}
