package za.ac.sun.cs.ingenious.core.time;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.io.Serializable;

import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;

/**
 * Provides a basic timer. Used for example in {@link GenActionMessage}s to specify how
 * much time is left to generate an action.
 * 
 * TODO This should be expanded with more sophisticated time controls, see issue #3
 */
public class Clock implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * The timeout before the clock starts, -1 defines unlimited time.
	 */
	private int timeout;
	
	/**
	 * The current system time in ms when the clock has been started.
	 */
	private long start;
	
	/**
	 * Creates a clock with infinite timeout.
	 * 
	 */
	public Clock() {
		this(-1);
	}
	
	/**
	 * Creates a clock with specified timeout
	 * @param timeout in ms
	 */
	public Clock(int timeout){
		this.timeout = timeout;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	public int getRemainingTimeMS(){
		if(timeout == -1){
			return Integer.MAX_VALUE;
		}
		return (int)((start + timeout) - System.currentTimeMillis());
	}
	
	public void start(){
		assert start == 0 : "Trying to startAsync clock twice";
		if(start == 0){
			start = System.currentTimeMillis();
		}else{
			Log.error("Clock was started already.");
			new NullPointerException().printStackTrace();
		}
	}
	
	 private void readObject(java.io.ObjectInputStream stream)
		     throws IOException, ClassNotFoundException{
	    stream.defaultReadObject();
	    start = System.currentTimeMillis();
	}
	 
}
