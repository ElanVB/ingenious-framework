package za.ac.sun.cs.ingenious.games.ingenious.engines;

import com.esotericsoftware.minlog.Log;

import java.awt.Color;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import edu.princeton.cs.introcs.StdDraw;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;

public class HumanEngine extends IngeniousEngine {

	private static IngeniousBoard gameBoard = new IngeniousBoard(11,6);
	private final static Color[] colours = new Color[] { StdDraw.RED,
			StdDraw.GREEN, StdDraw.BLUE, StdDraw.ORANGE, StdDraw.YELLOW,
			StdDraw.MAGENTA };
	private final static int HEX_CORNERS = 6;

	private static int rotation = 0;
	int selectedTile = 0;
	Coord nextPlacement = new Coord(0, 0);

	public HumanEngine(Socket socket,MatchSetting match, int playerId) {
		super(socket,match, playerId);

		initCanvas();
	}

	private void initScreen(IngeniousRack rack) {


		drawBlankBoard(1);
		StdDraw.point(0, 0);
		drawBoardUpdate();
		drawRack(rack, 0, 0);
	}

	public HumanEngine(String hostname, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
			super(new Socket(hostname, port),match, position);

			initCanvas();
	}

	private void initCanvas() {
		StdDraw.setCanvasSize(1000, 1000);
		StdDraw.setXscale(-20.0, 20.0);
		StdDraw.setYscale(-20.0, 20.0);
		drawEndTurnButton();
	}

	private void drawEndTurnButton() {
		StdDraw.filledRectangle(17, 15, 2, 1);
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.text(17, 15, "End Turn");
		StdDraw.setPenColor();
	}
	
//	public void run() {
//		int count = 0;
//
//		while (true) {
//			try {
//				String[] msg = messageHandler.receiveMessage();
//
//				if (msg[0].equals(TCPProtocol.ID)) {
//					messageHandler.reply("" + this.playerId);
//				}else if(msg[0].equals("setrack")){
//					Tile next;
//					for(int i =1;i<msg.length;i= i+2){
//			//			next = new Tile(Integer.parseInt(msg[i]),Integer.parseInt(msg[i+1]));
//			//			rack[i/2]= next;
//				//		Log.info("TILE ADDED TO RACK : "+next);
//					}
//					initScreen(this.racks.getInstance(playerId));
//				} else if (msg[0].equals(TCPProtocol.NAME)) {
//					messageHandler.reply("Human_Engine");
//				} else if (msg[0].equals(TCPProtocol.GENMOVE)) {
//
//					boolean moveMade = activate();
//
//					if (moveMade) {
//						IngeniousMove move = gameBoard.lastMove();
//						messageHandler.reply(
//								move.getTile().getTopColour() + "", move
//										.getTile().getBottomColour() + "", move
//										.getTile().getRotation() + "", move
//										.getPosition().getX() + "", move
//										.getPosition().getY() + "");
//					}
//				} else if (msg[0].equals(TCPProtocol.PLAYMOVE)) {
//					Log.info("playmove received");
//					String[] moveReply = msg;
//					Log.info(moveReply.length);
//
//					Tile tile = new Tile(Integer.parseInt(moveReply[1]),
//							Integer.parseInt(moveReply[2]));
//					tile.setRotation(Integer.parseInt(moveReply[3]), 6);
//					Coord coords = new Coord(
//							Integer.parseInt(moveReply[4]),
//							Integer.parseInt(moveReply[5]));
//					IngeniousMove move = new IngeniousMove(tile, coords);
//					gameBoard.makeMove(move);
//					Log.info(gameBoard);
//				}
//
//			} catch (Exception e) {
//				messageHandler.errorReply("" + e.toString());
//				e.printStackTrace();
//			}
//		}
//	}

	private boolean activate() {
		boolean guiActive = true;
		boolean moveMade = false;

		while (guiActive) {
			if (StdDraw.hasNextKeyTyped()) {
				if (StdDraw.nextKeyTyped() == 'r') {
					rotation++;
					rotation = rotation % 6;
					clearTileInfo();
					printTileInfo(rotation, selectedTile);

				}
			}
			if (StdDraw.mousePressed()) {

				double xMouse = StdDraw.mouseX();
				double yMouse = StdDraw.mouseY();
				Log.info(xMouse + "," + yMouse);
				if (xMouse < 11 && xMouse > -11) {
					if (yMouse < 10 && yMouse > -10) {
						if (!moveMade) {
							nextPlacement = pixel_to_hex(xMouse, -yMouse, 1);
							
						//	rack[selectedTile].setRotation(rotation, 6);
						/*	Move move = new Move(rack[selectedTile],nextPlacement);
							Log.info(move);
							if (gameBoard.makeMove(move)) {
								Log.info(move.getPosition());
								drawBoardUpdate();
								moveMade = true;
								Log.info("MOVE MADE");
							}
							*/
						}
						
					}
				}

				if (xMouse > -16 && xMouse < -4) {
					if (yMouse < -14 && yMouse > -19) {
						double position = xMouse + 15;
						position = position / 2;
						selectedTile = (int) Math.round(position);
						clearTileInfo();
						printTileInfo(rotation, selectedTile);
					}
				}
				if (xMouse > 15 && xMouse < 19) {
					if (yMouse > 14 && yMouse < 16) {
						guiActive = false;
					}
				}
				try {
					Thread.sleep(700);
				} catch (InterruptedException e) {
				}
			}
		}
		return moveMade;

	}

	/*
	 * http://www3.cs.stonybrook.edu/~skiena/392/lectures/week12/
	 */
	private double[] hexCorner(double x, double y, int size, int i) {
		double angle_deg = 60 * i + 30;
		double angle_rad = Math.PI / 180 * angle_deg;

		return new double[] { x + size * Math.cos(angle_rad),
				y + size * Math.sin(angle_rad) };
	}

	private void drawHex(double x, double y, int size, Color colour,
			boolean filled) {

		double[] xPositions = new double[6];
		double[] yPositions = new double[6];

		for (int i = 0; i < HEX_CORNERS; i++) {
			double[] corner = hexCorner(x, y, size, i);
			xPositions[i] = corner[0];
			yPositions[i] = corner[1];
		}
		if (colour != null) {
			StdDraw.setPenColor(colour);
		}
		if (filled) {
			StdDraw.filledPolygon(xPositions, yPositions);
		} else {
			StdDraw.polygon(xPositions, yPositions);
		}
		StdDraw.setPenColor();
	}

	private double[] hex_to_pixel(double q, double r, int size) {
		double x = (2.0 * size) * r * (1.0 / 2.0) + (2.0 * size) * q;
		double y = (2.0 * size) * r * (Math.sqrt(3) / 2.0);
		return new double[] { x , y };
	}

	private static Coord pixel_to_hex(double x, double y, int size) {
		// double q = (x * Math.sqrt(3) / 3 - y / 3) / size;
		// double r = y * 2 / 3 / size;
	//	x = -x;
	//	y = -y;
		double q = (2.0 / Math.sqrt(3)) * y / (2.0 * size);
		double r = (x - (2.0 * size) * q * (1.0 / 2.0)) / (2.0 * size);
		return Coord.hexRound(r, q);
	}

	private void drawBlankBoard(int size) {
		double pixel[] = new double[2];

		int prefixLength = gameBoard.SIDE_LENGTH - 1;
		int count = 0;
		boolean reachedMiddleLine = false;
		int k = 0;
		for (int q = -(gameBoard.SIDE_LENGTH - 1); q < (gameBoard.SIDE_LENGTH); q++) {

			if (prefixLength == 0) {
				reachedMiddleLine = true;
			}
			if (!reachedMiddleLine) {
				prefixLength--;

			} else {
				prefixLength++;
			}

			if (!reachedMiddleLine) {

				for (int r = k; r < gameBoard.SIDE_LENGTH; r++) {
					pixel = hex_to_pixel(r, q, 1);
					drawHex(pixel[0], pixel[1], 1, null, false);
				}
				k--;
			} else {
				for (int r = -(gameBoard.SIDE_LENGTH - 1); r < gameBoard.SIDE_LENGTH
						- prefixLength + 1; r++) {
					pixel = hex_to_pixel(r, q, 1);
					drawHex(pixel[0], pixel[1], 1, null, false);
				}
			}
			count++;
		}

	}

	private void printPlacementSelected(Coord coord) {
		StdDraw.text(10, -12, coord.toString());
	}

	private void printTileSelected(int position) {
		StdDraw.text(10, -14, "Tile Selected : " + position);
	}

	private void drawRack(IngeniousRack rack, int rot, int pos) {

		StdDraw.line(-20, -10, 20, -10);

		for (int i = 0; i < rack.size(); i++) {
			drawHex(-15 + i * 2, -15, 1, colours[rack.get(i).getTopColour()], true);
		}
		for (int i = 0; i < rack.size(); i++) {
			drawHex(-15 + i * 2, -17, 1, colours[rack.get(i).getBottomColour()], true);
		}

		printTileInfo(rot, pos);
	}

	private void printTileInfo(int rot, int position) {
		StdDraw.text(5, -12, "Toggle tile rotation by pushing 'r'");
		StdDraw.text(10, -16, "Rotation : " + rot);
		printTileSelected(position);
	}

	private void clearRack() {
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.filledRectangle(0, -15, 20, 5);
		StdDraw.setPenColor();
	}

	private void clearTileInfo() {

		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.filledRectangle(10, -15, 10, 4);
		StdDraw.setPenColor();
	}

	public static void main(String[] args) {
	//	HumanEngine humie = new HumanEngine(new Socket(),null, 0);
		//StdDraw.setCanvasSize(1000,1000);
		
		//Log.info(humie.pixel_to_hex(-3.2399999999999984,-5.120000000000001,1));
		//Log.info(humie.hex_to_pixel(0,1, 1)[0]+" , "+humie.hex_to_pixel(0,1, 1)[1]);
	}

	private void drawBoardUpdate() {
		double pixel[] = new double[2];

		for (Coord c : gameBoard.getCoordinates()) {
			pixel = hex_to_pixel(c.getX(), c.getY(), 1);
			drawHex(pixel[0], pixel[1], 1, colours[gameBoard.getHex(c)], true);
		}
		Log.info(gameBoard);
	}
}
