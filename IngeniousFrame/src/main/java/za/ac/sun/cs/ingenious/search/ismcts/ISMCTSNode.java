package za.ac.sun.cs.ingenious.search.ismcts;

import java.util.List;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;

/**
 * Type of search node to be used for ISMCTS. In addition to the fields of {@link SearchNode}, the
 * nodes also store their availability count.
 */
public class ISMCTSNode<S extends TurnBasedGameState> extends SearchNode<S, ISMCTSNode<S>> {

	protected int availabilityCount;
	
	public ISMCTSNode(S thisNodeState, GameLogic<S> logic, Move toThisNode, ISMCTSNode<S> parent, List<Move> unexpandedMoves) {
		super(thisNodeState, logic, toThisNode, parent, unexpandedMoves);
	}

	/**
	 * @return The number of times this node was available when the parent node was visited.
	 */
	public int getAvailabilityCount() {
		return availabilityCount;
	}

	/**
	 * Increase the value of getAvailabilityCount() by 1.
	 */
	public void increaseAvailabilityCount() {
		this.availabilityCount++;
	}
}
