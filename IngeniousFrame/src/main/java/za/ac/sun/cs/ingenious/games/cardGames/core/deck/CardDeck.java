package za.ac.sun.cs.ingenious.games.cardGames.core.deck;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The Class Deck.
 *
 * Basically just an ArrayList of cards.
 *
 * Gets two generic types which implement the CardFeature interface.
 * Also gets two Iterables of these generic types as parameters for the constructor.
 * 
 * The class Deck automatically creates a certain number cards (according to the optional parameter of the constructor - default: 1) for each combination of the two Iterables.
 * 
 * This class doesn't cover decks where the number of cards of one type differs from another card type. (e. g. 1 x queen of spades, 2 x king of hearts).
 *
 * @param <F1> Generic type of first feature (implements CardFeature).
 * @param <F2> Generic type of second feature (implements CardFeature).
 * 
 * @author Joshua Wiebe
 */
public class CardDeck<F1 extends CardFeature, F2 extends CardFeature>{
	
	/** The cards. */
	private ArrayList<Card<F1, F2>> cards;

	/**
	 * Instantiates a new deck.
	 *
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 * @param nrOfAppearancesPerCard the nr of appearances per card
	 */
	public CardDeck(Iterable<F1> feature1, Iterable<F2> feature2, int nrOfAppearancesPerCard) {

		initDeck(nrOfAppearancesPerCard, feature1, feature2);
	}

	/**
	 * Instantiates a new deck.
	 *
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 */
	public CardDeck(Iterable<F1> feature1, Iterable<F2> feature2) {
		this(feature1, feature2, 1);
	}

	/**
	 * Inits the deck.
	 *
	 * @param nrOfAppearancesPerCard the nr of appearances per card
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 */
	private void initDeck(int nrOfAppearancesPerCard, Iterable<F1> feature1, Iterable<F2> feature2) {
		cards = new ArrayList<>();

		for (F1 f1 : feature1) {
			for (F2 f2 : feature2) {
				Card<F1, F2> newCard = new Card<F1, F2>(f1, f2);

				// Create x actual cards of one type, where x is nrOfAppearancesPerCard.
				for (int i = 0; i < nrOfAppearancesPerCard; i++) {
					cards.add(newCard);
				}
			}
		}
	}

	/**
	 * Prints the deck.
	 */
	public void printDeck() {
		int i=1;
		for (Card<F1, F2> card: cards) {
			Log.info("Card nr " + (i++) + ": " + card.toString());
		}
	}
	
	/**
	 * Gets all cards - shallow copy.
	 *
	 * @return the all cards
	 */
	public ArrayList<Card<F1, F2>> getAllCards(){
		//Not a deep copy.
		return (ArrayList<Card<F1, F2>>) cards.clone();
	}
	
	/**
	 * Removes given card from Deck.
	 * If the card is not contained deck: sit idle & return false.
	 * @param cardToRemove
	 * @return If card was contained before.
	 */
	public boolean removeCardType(Card<F1, F2> cardToRemove){
		if(cards.contains(cardToRemove)){
			cards.remove(cardToRemove);
			return true;
		}
		
		return false;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		return cards.size();
	}
}
