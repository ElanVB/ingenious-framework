#!/bin/bash
# Here we start two random clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client


# Engine 1 is the simple rule based engine.s
# Engines 2 - 6 are random engines

java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsSimpleRuleBasedEngine" -game "Hearts" -hostname localhost -port 61234 &
java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsRandomEngine" -game "Hearts" -hostname 127.0.0.1 -port 61234 &
java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "josh" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsRandomEngine" -game "Hearts" -hostname localhost -port 61234 &
java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "michael" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsRandomEngine" -game "Hearts" -hostname localhost -port 61234 &
# java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "marvin" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsRandomEngine" -game "Hearts" -hostname localhost -port 61234 &
# java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "mark" -engine "za.ac.sun.cs.ingenious.games.cardGames.hearts.engines.HeartsRandomEngine" -game "Hearts" -hostname 127.0.0.1 -port 61234 &