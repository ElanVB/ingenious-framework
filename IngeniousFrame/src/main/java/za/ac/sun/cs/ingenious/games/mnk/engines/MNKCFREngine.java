package za.ac.sun.cs.ingenious.games.mnk.engines;

import com.esotericsoftware.minlog.Log;

import java.util.Map;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.cfr.CFR;
import za.ac.sun.cs.ingenious.search.cfr.Helper;

public class MNKCFREngine extends MNKEngine {
	
	CFR<MNKState> cfr;
	private final static int numIterations = 10;

	public MNKCFREngine(EngineToServerConnection toServer) {
		super(toServer);
		cfr = null;
	}

	@Override
	public String engineName() {
		return "MNKCFRngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		if (cfr == null) {
			cfr = new CFR<MNKState>(
				board,
				logic, logic,
				new MNKFinalEvaluator(),
				logic);
			cfr.solve(numIterations);			
		}
		
		Map<Action, Double> strat = Helper.getAverageStrategyProfile(
				cfr.getCumulativeStrategy(logic.observeState(board, board.nextMovePlayerID)));
		Log.info("Average profile at current state:");
		Log.info(strat);

		return new PlayActionMessage(Helper.sampleFromStrategy(strat));
	}

}
