package za.ac.sun.cs.ingenious.search.ismcts;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;

/**
 * Type of search node to be used for MOISMCTS. For MOISMCTS, each player is modeled by a separate
 * search tree. One object of this class therefore stores a list of the nodes that are currently
 * visited in each of the players' nodes.
 * Use this with {@link MOISMCTSDescender} and {@link MOISMCTSUpdater}.
 */
public class MOISMCTSNode<S extends TurnBasedGameState> extends SearchNode<S, MOISMCTSNode<S>> {

	protected List<ISMCTSNode<S>> playerNodes;
	
	/**
	 * @param currentPlayer The player active at this node (this should be the same as the result of
	 * 						playerNodes.get(k).getCurrentPlayer() for any k).
	 * @param playerNodes The nodes in each of the players' trees. Note that the game state and list of
	 * 					  unexpanded moves in this MOISMCTSNode object will be set to the state and list
	 * 					  of playerNodes.get(currentPlayer).
	 * @param logic Object containing the game logic.
	 * @param toThisNode Action leading to this node.
	 * @param parent Parent of this node.
	 */
	public MOISMCTSNode(int currentPlayer, List<ISMCTSNode<S>> playerNodes, GameLogic<S> logic,
			Action toThisNode, MOISMCTSNode<S> parent) {
		super(playerNodes.get(currentPlayer).getGameState(),
				logic, toThisNode, parent, playerNodes.get(currentPlayer).getUnexpandedMoves());
		this.playerNodes = playerNodes;
	}
	
	/**
	 * @return The root node of a MOISMCTS search tree for the given information set. This node can then be
	 * 		   supplied to {@link MCTS}.
	 */
	public static <S extends TurnBasedGameState> MOISMCTSNode<S> createRootNode(int searchingPlayer,
			S searchingPlayerInformationSet, GameLogic<S> logic, InformationSetDeterminizer<S> det,
			ActionSensor<S> sensor) {
		// Create game trees the information sets for the given state of each player
		List<ISMCTSNode<S>> playerTrees = new ArrayList<ISMCTSNode<S>>();
		for (int i=0; i<searchingPlayerInformationSet.getNumPlayers(); i++) {
			// For the searching player, add the available actions as unexpanded moves. For the others, we
			// don't need to add unexpanded nodes as MOISMCTS always expands from the tree of the currently
			// active player.
			if (i == searchingPlayer) {
				playerTrees.add(new ISMCTSNode<S>(searchingPlayerInformationSet, logic, null, null,
						new ArrayList<Move>(logic.generateActions(searchingPlayerInformationSet, searchingPlayer))));
			} else {
				S infoSetForI = det.observeState(searchingPlayerInformationSet, i);
				playerTrees.add(new ISMCTSNode<S>(infoSetForI, logic, null, null,
						null));				
			}
		}
		return new MOISMCTSNode<>(searchingPlayerInformationSet.nextMovePlayerID,
				playerTrees, logic, null, null);
	}

	/**
	 * @return The nodes for each player that are active at this state of the search.
	 */
	public List<ISMCTSNode<S>> getPlayerNodes() {
		return playerNodes;
	}
	
	/**
	 * The availability count is increased by 1 in each of the players' nodes.
	 */
	public void increaseAvailabilityCount() {
		for (ISMCTSNode<S> n : playerNodes) {
			n.increaseAvailabilityCount();
		}
	}
}
