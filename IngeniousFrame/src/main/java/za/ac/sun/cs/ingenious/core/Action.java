package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;

/**
 * Represents one action in a game. An action is any activity of some player
 * that changes the game state. For instance, in a card game, "play ten of
 * hearts" may be an action. Similarly, in TicTacToe, "place your mark in the
 * central spot" is an action. An action may look differently, depending on
 * which player observes it. For that, see {@link Move}.
 * 
 * Actions are sent by a player to the server using {@link PlayActionMessage}s.
 * 
 * Game developers can create classes for actions that are possible in their
 * game by implementing this interface. For an example, see {@link XYAction}.
 */
public interface Action extends Move {

}
