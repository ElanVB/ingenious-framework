package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

public class DomineeringFinalEvaluator implements GameFinalEvaluator<TurnBasedSquareBoard> {

	private int getWinner(TurnBasedSquareBoard forState) {
		return (forState.nextMovePlayerID==0?1:0);
	}

	@Override
	public double[] getScore(TurnBasedSquareBoard forState) {
		double[] scores = new double[2];
		int winner = getWinner(forState);
		scores[winner] = 1;
		return scores;
	}

}
