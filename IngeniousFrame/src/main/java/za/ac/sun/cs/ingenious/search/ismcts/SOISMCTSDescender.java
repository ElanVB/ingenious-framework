package za.ac.sun.cs.ingenious.search.ismcts;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.SearchValue;
import za.ac.sun.cs.ingenious.search.mcts.TreeDescender;

/**
 * Implementation of TreeDescender for SOISMCTS. First, chooses a determinization compatible
 * with the current information set. Then, always searches for the node with the
 * best search value until an expandable node is reached, but restricts nodes to descend and to 
 * expand to those that are available for the current determinization.
 */
public class SOISMCTSDescender<S extends TurnBasedGameState> implements TreeDescender<S, ISMCTSNode<S>> {

	protected GameLogic<S> logic;
	protected SearchValue<S,ISMCTSNode<S>> searchValue;
	protected int treeObserverID;
	protected ActionSensor<S> sensor;
	
	protected S determinizedState;
	protected InformationSetDeterminizer<S> det;
	
	/**
	 * @param logic Logic object.
	 * @param searchValue Object with which to generate search values for nodes. Most common is {@link SubsetUCT}
	 * @param treeObserverID ID of the player whom the information sets in this search tree belong to.
	 * @param sensor ActionSensor for actions that are being applied to states. Actions applied to the information
	 * 				 sets at each node are observed for the player with treeObserverID. Actions applied
	 * 				 to the determinization are observed as for the environment player.
	 * @param det Determinizer for information sets.
	 */
	public SOISMCTSDescender(GameLogic<S> logic, SearchValue<S, ISMCTSNode<S>> searchValue,
			ActionSensor<S> sensor, int treeObserverID, InformationSetDeterminizer<S> det) {
		this.logic = logic;
		this.treeObserverID = treeObserverID;
		this.sensor = sensor;
		this.searchValue = searchValue;
		
		this.det = det;
		this.determinizedState = null;
	}

	@Override
	public ISMCTSNode<S> expand(ISMCTSNode<S> parent) {
		// The simplest implementation just chooses a random move, but...
		List<Move> allMoves = parent.getUnexpandedMoves();
		List<Move> unexpandedMoves = new LinkedList<Move>();
		// ... restrict to moves that are available for the current determinization.
		for (Move m : allMoves) {
			if (logic.validMove(determinizedState, m)) {
				unexpandedMoves.add(m);
			}
		}
		if (unexpandedMoves.isEmpty()) {
			return null;
		}
		int index = (int) (Math.random() * unexpandedMoves.size());
		Move toExpand = unexpandedMoves.get(index);
		allMoves.remove(toExpand);
		
		updatingDeterminizedState(parent, toExpand);

		ISMCTSNode<S> child = createNodeForMove(toExpand, parent);
		parent.addChild(child);
		return child;
	}
	
	/**
	 * @param parent Node that is being expanded or descended.
	 * @param toExpand Move that is being expanded or applied.
	 */
	protected void updatingDeterminizedState(ISMCTSNode<S> parent, Move toExpand) {
		if (!(toExpand instanceof Action)) {
			String errorMsg = "Updating determinizedState with move that is not an action";
			Log.error("SOISMCTSDescender", errorMsg);
			throw new RuntimeException(errorMsg);
		}
		Action a = (Action) toExpand;
		logic.makeMove(determinizedState, sensor.fromPointOfView(a, determinizedState, -1));
	}

	/**
	 * @param move Move that leads to the new node.
	 * @param parent Parent of the new node.
	 * @return New child node of parent.
	 */
	protected ISMCTSNode<S> createNodeForMove(Move move, ISMCTSNode<S> parent) {
		if (!(move instanceof Action)) {
			String errorMsg = "Expanding move that is not an action.";
			Log.error("SOISMCTSDescender", errorMsg);
			throw new RuntimeException(errorMsg);
		}
		Action a = (Action) move;
		
		@SuppressWarnings("unchecked")
		S newState = (S) parent.getGameState().deepCopy();
		this.logic.makeMove(newState, this.sensor.fromPointOfView(a, newState, treeObserverID));
		return new ISMCTSNode<S>(newState, logic, move, parent,
				new ArrayList<Move>(logic.generateActions(newState, newState.nextMovePlayerID)));
	}
	

	@Override
	public ISMCTSNode<S> descend(ISMCTSNode<S> root, int totalNofPlayouts) {		
		// First, determinize the root state
		determinizedState = det.determinizeUnknownInformation(root.getGameState(), treeObserverID);

		// Now takes a greedy path trough the tree by always picking 
		// the node with the highest searchValue until it hits an expandable or terminal node.
		// However, only nodes that are available for the current determiniziation are considered.
		// Furthermore, the determinization is updated before the search continues.
		ISMCTSNode<S> currentNode = root;
		while(currentNode != null && !currentNode.isTerminal()){
			if (!currentNode.getUnexpandedMoves().isEmpty() ||
					currentNode.getExpandedChildren().isEmpty()) {
				// Node can be expanded OR no further descent possible, therefore descent ends here.
				break;
			}
			
			double highestValue = Double.NEGATIVE_INFINITY;
			ISMCTSNode<S> bestChild = null;
			for(ISMCTSNode<S> child : currentNode.getExpandedChildren()){
				if (!logic.validMove(determinizedState, child.getMove())) {
					continue;
				}
				// For subset multi armed bandit, update the availability count of each child node
				// that is available during this descent.
				child.increaseAvailabilityCount();
				double childValue = this.searchValue.get(child, currentNode.getCurrentPlayer(), totalNofPlayouts);
				if(childValue > highestValue){
					bestChild = child;
					highestValue = childValue; 
				}
			}

			if (bestChild == null) {
				break;
			} else {
				updatingDeterminizedState(currentNode, bestChild.getMove());
				currentNode = bestChild;
			}
		}	
		return currentNode;
	}

	@Override
	public S getStateForPlayout(ISMCTSNode<S> fromNode) {
		return determinizedState;
	}
	
}
