package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

/**
 * The class UnoFinalEvaluator.
 * 
 * Evaluates a given game state.
 * Normally used when a game state is terminal.
 * 
 * @author Joshua Wiebe
 */
public class UnoFinalEvaluator implements GameFinalEvaluator<UnoGameState>{
	
	/**
	 * @see za.ac.sun.cs.ingenious.core.GameFinalEvaluator#getWinner(za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * Very similar to the isTerminal method of UnoLogic
	 * @return Returns the winner ID of a given state. If there is no identifiable winner: return -1.
	 */
	public int getWinner(UnoGameState forState) {
		
		// The winner
		int winner;
		
		// Fetch map from game state for further inspection.
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = forState.getMap();
		
		// ArrayList to store different locations.
		ArrayList<UnoLocations> list = new ArrayList<>();
		
		// For each card type
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			
			// For each location of a card type.
			for (UnoLocations location : entry.getValue()) {
				
				// If list doesn't contain location: add it.
				if (!list.contains(location)) {
					list.add(location);
				}
			}
		}
		
		// In a terminal state there can be only one player without any cards.
		// Therefore check search for the one player who's ID is not in the ArrayList generated above.
		for(int i=0; i<forState.getNumPlayers(); i++){
			if(!list.contains(UnoLocations.values()[i])){
				// Update field
				winner = i;
				return winner;
			}
		}
		
		// Update field.
		winner = -1;
		return winner;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameFinalEvaluator#getScore(za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * @return Returns the scorings as ArrayList (-1 for loss, 1 for win).
	 */
	@Override
	public double[] getScore(UnoGameState forState) {
		
		// Int-array for score. 
		double[] scores = new double[forState.getNumPlayers()];
		
		// Get the winner
		int winner = getWinner(forState);
		
		// If no winner exists return each player gets the score 0.
		if(winner<0){
			Arrays.fill(scores, 0);
			return scores;
		}
		
		// Assign -1 to every player.
		Arrays.fill(scores, -1);
		
		// Update only the score of the winner with scoring of 1. (In Uno there can only be one)
		scores[winner] = 1;
		
		return scores;
	}
}
