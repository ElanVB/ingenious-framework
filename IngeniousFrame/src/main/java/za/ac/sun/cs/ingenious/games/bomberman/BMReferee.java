package za.ac.sun.cs.ingenious.games.bomberman;

import com.esotericsoftware.minlog.Log;
import com.google.common.eventbus.EventBus;

import java.util.Map;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.GameSystem;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMFinalEvaluator;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.IdleAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.PlaceBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.TriggerBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGameTerminateMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMInitGameMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NewUpgradesMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NextRoundMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;
import za.ac.sun.cs.ingenious.games.bomberman.ui.BombermanFrame;

public class BMReferee extends FullyObservableMovesReferee<BMBoard, BMLogic, BMFinalEvaluator> {

    /** Time per move in milliseconds; -1 indicates no limit */
    private static final int TIME_PER_ROUND = 2000;
    /** Time in milliseconds after all players connected before game starts */
    private static final int GAME_START_DELAY = 2000;
    /** Time paused between rounds - useful for observing games */
    private static final int BETWEEN_ROUND_DELAY = 1000;
    /** When requesting moves in parallel, the time between successive checks to see if all players have submitted moves. */
    private static final int CHECK_MOVE_INTERVAL = 1000;

	public BMReferee(MatchSetting match, PlayerRepresentation[] players) throws IncorrectSettingTypeException {
		super(match, players,
				new BMBoard(match.getSettingAsInt("boardSize", BMBoard.DEFAULT_BOARD_SIZE),
						players.length,
						match.getSettingAsBoolean("perfectInformation", false),
						match.getSettingAsBoolean("alternatingPlay", false))
				, BMLogic.defaultBMLogic, new BMFinalEvaluator());
        if (match.getSettingAsBoolean("uiOn", true)) {
			new BombermanFrame(this);
        }
	}
	
	@Override
	protected void beforeGameStarts() {
		updateUI();
	}
	
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new BMInitGameMessage((char) ('A' + player.getID()), player.getID(), currentState);
	}
	
	// Used to send environment actions
	// TODO This should be done with an actual environment player. The class PlayedMoveMessage should not be extended, see issue #139
	protected void distributeMove(PlayedMoveMessage move) {
		for (int i = 0; i < players.length; i++) {
				players[i].playMove(move);
		}
	}
	
	@Override
	protected void beforeRound() {
        Log.info("=============================================");
        Log.info("                  Round " + currentState.getRound());
        Log.info("=============================================");
    
		// Update bombs and release resulting upgrades
        // TODO These should be actions made by the environment, see issue #139
        int updatePlayer = -1;
        if (currentState.isAlternating()) {
        	updatePlayer = currentState.alternatingMovingPlayer;
        }
		logic.applyPlayedMoveMessage(new UpdateBombsMessage(updatePlayer, true), currentState);
		distributeMove(new UpdateBombsMessage(updatePlayer, false));
		distributeMove(new NewUpgradesMessage(currentState.getVisibleBlastUpgrades(), currentState.getVisibleBombUpgrades()));
	}
	
	@Override
	protected GenActionMessage createGenActionMessage(PlayerRepresentation player) {
		return new BMGenActionMessage(player.getID(), currentState.getRound(), TIME_PER_ROUND);
	}
	
	@Override
	protected void reactToNullAction(int player, PlayActionMessage m) {
		super.reactToNullAction(player, m);
		logic.nextPlayer(currentState);
	}
	
	@Override
	protected void reactToInvalidAction(int player, PlayActionMessage m) {
		super.reactToInvalidAction(player, m);
		logic.nextPlayer(currentState);
	}
	
	@Override
	protected boolean checkActionAllowedForPlayer(BMBoard fromState, Action action, int id) {
		//idle move is always valid
        if(action instanceof IdleAction) {
            return true; 
        }
		if(action instanceof TriggerBombAction){
            return ((TriggerBombAction) action).getPlayerID() == id;
		}
		if (action instanceof PlaceBombAction) {
			PlaceBombAction m = (PlaceBombAction) action;
			return (m.getPlayerID() == id);
		} else if (action instanceof CompassDirectionAction) {
			CompassDirectionAction m = (CompassDirectionAction) action;
			return (m.getPlayerID() == id);
		}
		return false;
	}
	
	// TODO It shouldn't be necessary to override this. The GeneralReferee simply calls logic.makeMove at this point, but because Bomberman uses different PlayedMoveMessages this method must be overridden. This will become much cleaner by solving issue #139
	@Override
	protected void updateServerState(PlayActionMessage m) {
        logic.applyPlayedMoveMessage(new PlayedMoveMessage(m), currentState);
	}
	
	@Override
	protected void afterRound(Map<Integer, PlayActionMessage> messages) {
		super.afterRound(messages);

		// For alternating play, must make sure the NextRoundMessage is only sent once
		if (!currentState.isAlternating() || currentState.alternatingMovingPlayer == 0) {
			// Send NextRoundMessage
	        // TODO This should be an action made by the environment, see issue #139
			NextRoundMessage nr = new NextRoundMessage();
		    logic.applyPlayedMoveMessage(nr, currentState);
		    distributeMove(nr);
		}
		
        Log.info("Scores:");
        for (int i = 0; i < currentState.getScores().length; i++) {
            Log.info("	Player " + i + ": " + currentState.getScores()[i] + "		("
                                       + currentState.getPlayer(i).getNofBombs() + "/"
                                       + currentState.getPlayer(i).getMaxNofBombs() + "|"
                                       + currentState.getPlayer(i).getBlastRadius() + ")");
        }
        updateUI();

        pause(BETWEEN_ROUND_DELAY);
	}
	
	@Override
	protected GameTerminatedMessage createTerminateGameMessage(PlayerRepresentation player) {
		return new BMGameTerminateMessage(eval.getScore(currentState));
	}
	
	@Override
	protected void afterGameTerminated() {
		super.afterGameTerminated();

        // Post result on event bus for publish-subscribe system.
        // Amongst other things, used for communicating result to tournament engine.
        EventBus bus = GameSystem.getInstance().getEventBus();
        bus.post(new BMGameTerminateMessage(eval.getScore(currentState)));
	}
	

    /** Update GUI, if activated. */
    private void updateUI() {
        setChanged();
        notifyObservers(currentState.asCharArray());
    }

    /**
     * Pause the given number of milliseconds, or until interrupted (which should not be done).
     */
    private void pause(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Log.warn("Bomberman Referee", "Unexpected interruption during pause", e);
        }
    }
    
    /** Returns the current game state */
    public BMBoard getBoard() {
        return currentState;
    }

}
