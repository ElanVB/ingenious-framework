package za.ac.sun.cs.ingenious.games.mnk.engines;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.ismcts.ISMCTSNode;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSDescender;
import za.ac.sun.cs.ingenious.search.ismcts.SubsetUCT;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;

public class MNKSOISMCTSEngine extends MNKEngine {

	public MNKSOISMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public String engineName() {
		return "MNKSOISMCTSEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		ISMCTSNode<MNKState> root = new ISMCTSNode<MNKState>(board, logic, null, null,
				new ArrayList<Move>(logic.generateActions(board, board.nextMovePlayerID)));
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<MNKState>(logic, new MNKFinalEvaluator(), logic, false),
				new SOISMCTSDescender<MNKState>(logic, new SubsetUCT<>(), logic, playerID, logic),
				new SimpleUpdater<ISMCTSNode<MNKState>>(),
				Constants.TURN_LENGTH));
	}

}
