package za.ac.sun.cs.ingenious.games.cardGames.hearts.engines;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsGameState;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsInitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsLocations;

/**
 * A random Hearts engine.
 * 
 * @author Joshua Wiebe
 */
public class HeartsRandomEngine extends Engine {

	/** A HeartsGameState for internal representation. */
	HeartsGameState state;

	/** A HeartsGameLogic for internal representation. */
	HeartsGameLogic logic;

	/**
	 * Instantiates a new Hearts random engine.
	 *
	 * @param toServer
	 *            the server connection
	 */
	public HeartsRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
		logic = new HeartsGameLogic();
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#engineName()
	 * 
	 * @return Returns it's name.
	 */
	@Override
	public String engineName() {
		return "HeartsRandomPlayer";
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveInitAction(za.ac.sun.cs.ingenious.core.network.game.actions.InitGameAction)
	 * 
	 *      Initialize the game state and update it with the given CardRack.
	 * 
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		
		// Cast InitGameAction to HeartsInitGameAction
		HeartsInitGameMessage higa = (HeartsInitGameMessage) a;

		// Retrieve CardRack
		CardRack<ClassicalSymbols, ClassicalSuits> rack = higa.getRack();

		// Initialize the game state with a Hearts deck.
		state = new HeartsGameState(higa.getNumPlayers());

		// Update game state according to given rack.
		Map<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map = state.getMap();
		for (Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : map.entrySet()) {
			Card<ClassicalSymbols, ClassicalSuits> card = entry.getKey();
			
			// If card is on initial rack of this player: change to player location.
			if (rack.contains(card)) {
				try {
					state.changeLocation(card, HeartsLocations.DRAWPILE, HeartsLocations.values()[super.playerID]);
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			}
			
			// Else: change location to UNKNOWNPLAYER
			else {
				try {
					state.changeLocation(card, entry.getValue().get(0), HeartsLocations.UNKNOWNPLAYER);
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receivePlayMove(za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 * 
	 *      Update internal state according to received PlayMoveAction.
	 * 
	 *      Cannot simply use makeMove() of HeartsGameLogic because of imperfect
	 *      information.
	 * 
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage m) {
		Move move = m.getMove();

		PlayCardAction<HeartsLocations> pcMove = (PlayCardAction) move;

		// Update current player of state to make use of makeMove. (Need to trust the referee)
		state.setCurrentPlayer(pcMove.getPlayerID());
		
		// This player
		if (pcMove.getPlayerID() == this.getPlayerID()) {
			logic.makeMove(state, pcMove);
		}

		// Other player
		else {
			try {
				// Update state
				state.changeLocation(pcMove.getCard(), HeartsLocations.UNKNOWNPLAYER, HeartsLocations.values()[pcMove.getPlayerID()]);
				logic.makeMove(state, pcMove);
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGenerateMove(za.ac.sun.cs.ingenious.core.network.game.actions.GenMoveAction)
	 * 
	 * Generate random move.
	 * 
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		
		// Fetch all possible moves.
		ArrayList<Action> actions = (ArrayList<Action>) logic.generateActions(state, this.playerID);
		
		// If no possible moves (should never be the case).
		if (actions.size() == 0) {
			return null;
		}
				
		// Else: chose random move.
		Random rand = new Random();
		Action randomAction = actions.get(rand.nextInt(actions.size()));
		
		return new PlayActionMessage(randomAction);
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGameTerminatedAction(za.ac.sun.cs.ingenious.core.network.game.actions.GameTerminatedAction)
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
	}

	/**
	 * Print the current rack sizes of the state.
	 */
	public void printRackSizes() {
		StringBuilder s = new StringBuilder();
		s.append("Round: " + state.getRoundNR() + "\n");
		s.append("Racks: [");
		for (int i = 0; i < state.getNumPlayers(); i++) {
			if (i != 0) {
				s.append(", ");
			}
			s.append(state.getCopyOfNrTricksOfPlayers()[i]);
		}
		s.append("] \n");

		Log.info(s);
	}
}
