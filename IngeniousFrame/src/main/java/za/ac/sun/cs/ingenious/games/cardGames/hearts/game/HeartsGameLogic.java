package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;

/**
 * Game logic for Hearts.
 * 
 * @author Joshua Wiebe
 */
public class HeartsGameLogic implements TurnBasedGameLogic<HeartsGameState>{

	@Override
	public boolean validMove(HeartsGameState fromState, Move move) {
		// Declare all conditions for a valid move and initialize with false.
		boolean validPlayer = false, sameOldLocation = false, validNewLocation = false, 
				compatibleWithTrickSuit = false, firstRoundNoPoints = false, validStart = false;
		
		// Must be a PlayCardMove
		if(!(move instanceof PlayCardAction)){
			return false;
		}
		
		// Convert to PlayCardMove
		PlayCardAction<HeartsLocations> pcm = (PlayCardAction) move;
		
		// Fetch card-location-map
		TreeMap<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map = fromState.getMap();
		
		// Check if it's the right players turn & origin.
		validPlayer = pcm.getPlayerID() == fromState.getCurrentPlayer() && pcm.getOldLoc() == HeartsLocations.values()[pcm.getPlayerID()];
		
		// Card of the first move must be the startingCard
		if(fromState.getRoundNR()==1 && fromState.getCurrentTrick().isEmpty()){
			validStart = fromState.getStartingCard().equals(pcm.getCard());
		} else{
			validStart = true;
		}

		// Check if old location of card exists in gameState.
		if (pcm.getOldLoc().equals(map.get(pcm.getCard()).get(0))) {
			sameOldLocation = true;
		}

		// Check if compatible with current trick suit.
		
		// If current trick suit is null: player has free choice with the restriction that heart was already played before.
		if (fromState.getCurrentTrickSuit() == null) {
			compatibleWithTrickSuit = !pcm.getCard().getf2().equals(ClassicalSuits.HEARTS) || fromState.isHeartIsPlayed() || isOnlyHeartsOnPlayershand(fromState, pcm.getPlayerID());
		} 
		// Approve if the suit is valid.
		else if(pcm.getCard().getf2().equals(fromState.getCurrentTrickSuit())) {
			compatibleWithTrickSuit = true;
		} 
		// If the suit is not valid check if the player doesn't have the current trick suit on his hand.
		else{
			boolean playerLied = false;
			
			// For each card type
			for(Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : map.entrySet()){
				// If current player holds this card..
				if(entry.getValue().get(0).equals(HeartsLocations.values()[fromState.getCurrentPlayer()])){
					// If this card is of the 
					if(entry.getKey().getf1().equals(fromState.getCurrentTrickSuit())){
						playerLied = true;
					}
				}
			}
			// Is compatible with trick suit if player indeed doesn't hold the current trick suit.
			compatibleWithTrickSuit = !playerLied ;
		}

		// Check if new location is valid
		// Only moves where the origin is one of the players are valid.
		if (pcm.getOldLoc().ordinal() < fromState.getNumPlayers()) {
			validNewLocation = pcm.getNewLoc().equals(HeartsLocations.TRICKPILE);
		}
		
		// Check if points are played in the first round.
		firstRoundNoPoints = fromState.getRoundNR() > 1 || !(pcm.getCard().getf1().equals(ClassicalSymbols.QUEEN) && pcm.getCard().getf2().equals(ClassicalSuits.SPADES));
		
		// All constrains must be satisfied.
		boolean validMove = validPlayer && validStart && sameOldLocation && validNewLocation && compatibleWithTrickSuit && firstRoundNoPoints;

		return validMove;
	}

	@Override
	public boolean makeMove(HeartsGameState fromState, Move move) {
		
		// Check if valid move.
		if(!validMove(fromState, move)){
			System.out.println("Invalid move!!!");
			return false;
		}
				
		// Convert to PlayCardMove (checked in validMove() )
		PlayCardAction<HeartsLocations> pcm = (PlayCardAction<HeartsLocations>) move;
		
		// Update card Location
		try {
			fromState.changeLocation(pcm.getCard(), pcm.getOldLoc(), pcm.getNewLoc());
		} catch (KeyNotFoundException | LocationNotFoundException e) {
			e.printStackTrace();
		}
		
		// If it's the first move of the trick: update trick suit.
		if(fromState.getCurrentTrick().isEmpty()){
			fromState.setCurrentTrickSuit((ClassicalSuits) pcm.getCard().getf2());
		}
		
		// Update trick
		fromState.addCardToCurrentTrick(pcm.getCard());
						
		// If heart played: set heartIsPlayed flag.
		if(pcm.getCard().getf2().equals(ClassicalSuits.HEARTS)){
			fromState.setHeartIsPlayed(true);
		}
		
		// If first move of round:
		if(fromState.getCurrentTrick().size()==1){
			fromState.setCurrentTrickSuit((ClassicalSuits) pcm.getCard().getf2());
			// If first move of game: 
			if(fromState.getRoundNR() == 1){
				fromState.setBeginningPlayer(pcm.getPlayerID());
			}
		}
		
		// If round is complete: prepare for new round.
		if(fromState.getCurrentTrick().size() == fromState.getNumPlayers()){
			// Evaluate winner.
			int winner = determineTrickOwner(fromState);
			// Add all cards from this trick to winners trick pile.
			for(Card<ClassicalSymbols, ClassicalSuits> card : fromState.getCurrentTrick()){
				fromState.addCardToPlayersTrickPile(card, winner);
			}
			
			// Update trick pile tracking.
			fromState.incrementNrTricksOfPlayers(winner);
			
			// Reset currentTrick.
			fromState.resetCurrentTrick();
			
			// Update trick suit of last round
			fromState.setTrickSuitOfLastRound(fromState.getCurrentTrickSuit());
			
			// Reset currentTrickSuit.
			fromState.setCurrentTrickSuit(null);
			
			// Save the winner of last round
			fromState.setWinnerOfLastRound(fromState.getBeginningPlayer());
			
			// Set beginning player for next round which is also the current player then.
			fromState.setBeginningPlayer(winner);
			fromState.setCurrentPlayer(winner);
			
			// Update RoundNR
			fromState.incrementRoundNR();
			
		} else{
			nextTurn(fromState, move);
		}
		
		// Return success
		return true;
	}

	@Override
	public void undoMove(HeartsGameState fromState, Move move) {
		throw new UnsupportedOperationException("undo operation not supported in this game.");
	}

	@Override
	public List<Action> generateActions(HeartsGameState fromState, int forPlayerID) {
		
		// ArrayList for all possible moves generated.
		ArrayList<PlayCardAction<HeartsLocations>> actions = new ArrayList<>();
		
		// Fetch players location in the location enum.
		HeartsLocations playerLocation = HeartsLocations.values()[forPlayerID];
		
		// Fetch card-location-map which.
		TreeMap<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map = fromState.getMap();
		
		// If first move of the game: only possible card to play is the starting card.
		if(fromState.getRoundNR()==1 && fromState.getCurrentTrick().isEmpty()){
			// if player has this card: return it
			if(map.get(fromState.getStartingCard()).get(0).equals(HeartsLocations.values()[forPlayerID])){
				actions.add(new PlayCardAction<HeartsLocations>(forPlayerID, fromState.getStartingCard(), 
						HeartsLocations.values()[forPlayerID], HeartsLocations.TRICKPILE));
				return (List) actions;
			}
		}

		// Get current suit
		ClassicalSuits currentTrickSuit = fromState.getCurrentTrickSuit();

		// Flag if player can serve the current trickSuit
		boolean ableToServe = false;
		
		// For each card type
		for (Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : map.entrySet()) {
			Card<ClassicalSymbols, ClassicalSuits> card = entry.getKey();

			// Get players cards.
			for (HeartsLocations location : entry.getValue()) {
				if (location.equals(playerLocation)) {
					actions.add(new PlayCardAction<HeartsLocations>(forPlayerID, card, playerLocation,
							HeartsLocations.TRICKPILE));
					if(card.getf2().equals(currentTrickSuit)){
						ableToServe = true;
					}
				}
			}
		}
		
		// Filter if player can serve the current trick suit.
		if(ableToServe){
			for (Iterator<PlayCardAction<HeartsLocations>> it = actions.iterator(); it.hasNext();) {
			    PlayCardAction<HeartsLocations> move = it.next();
			    if (!move.getCard().getf2().equals(currentTrickSuit)) {
			        it.remove();
			    }
			}
		}
		
		// Filter if it's the first round (no points).
		else if(fromState.getRoundNR() == 1){
			for (Iterator<PlayCardAction<HeartsLocations>> it = actions.iterator(); it.hasNext();) {
			    PlayCardAction<HeartsLocations> move = it.next();
			    Card<ClassicalSymbols, ClassicalSuits> card = move.getCard();
			    if (card.getf2().equals(ClassicalSuits.HEARTS) || (card.getf1().equals(ClassicalSymbols.QUEEN) && card.getf2().equals(ClassicalSuits.SPADES))) {
			        it.remove();
			    }
			}
		}
		
		// Also handle hearts. You cannot start a round with hearts if it hasn't been played before.
		else if(!fromState.isHeartIsPlayed() && fromState.getCurrentTrick().isEmpty() && !isOnlyHeartsOnPlayershand(fromState, forPlayerID)){
			for (Iterator<PlayCardAction<HeartsLocations>> it = actions.iterator(); it.hasNext();) {
			    PlayCardAction<HeartsLocations> move = it.next();
			    if (move.getCard().getf2().equals(ClassicalSuits.HEARTS)) {
			        it.remove();
			    }
			}
		}
		return (List) actions;
	}

	@Override
	public boolean isTerminal(HeartsGameState state) {
		// Return if current round number if higher than the max round number.
		return state.getRoundNR() > state.getRoundsThatWillBePlayed();
	}
	
	/**
	 * Helper function to determine which player played the highest card in the current trick.
	 * @param fromState Hearts game state.
	 * @return The id of the player which played the highest card.
	 */
	private int determineTrickOwner(HeartsGameState fromState){
		
		// Fetch state properties.
		ArrayList<Card<ClassicalSymbols, ClassicalSuits>> trick = fromState.getCurrentTrick();		
		ClassicalSuits currentTrickSuit = fromState.getCurrentTrickSuit();
		
		// Winner is nothing good in this case.
		Card<ClassicalSymbols, ClassicalSuits> highestCard = trick.get(0);		
		
		for(Card<ClassicalSymbols, ClassicalSuits> card : trick){
			if(card.getf2().equals(currentTrickSuit) && highestCard.compareTo(card) < 0){
				highestCard = card;
			}
		}
		
		// return position (id) of player.
		return (trick.indexOf(highestCard) + fromState.getBeginningPlayer()) % fromState.getNumPlayers();
	}
	
	/**
	 * Sets the next player.
	 * @param fromState Hearts game state.
	 * @param move Move that was received.
	 */
	@Override
	public void nextTurn(HeartsGameState fromState, Move move){
		int nextPlayer;
		if(fromState.getCurrentTrick().isEmpty()){
			nextPlayer = fromState.getWinnerOfLastRound();
		}
		
		else{
			nextPlayer = (fromState.getCurrentPlayer() + 1) % fromState.getNumPlayers();
		}
		
		fromState.setCurrentPlayer(nextPlayer);
	}
	
	@Override
	public Set<Integer> getCurrentPlayersToAct(HeartsGameState fromState) {
		if (!fromState.isGameStarted()) {
			fromState.nextMovePlayerID = determineStartingPlayer(fromState);
			fromState.setGameStarted(true);
		}
		return TurnBasedGameLogic.super.getCurrentPlayersToAct(fromState);
	}
	
	/**
	 * 
	 * @param fromState HeartsGameState
	 * @return player who holds the starting card. -1 if no valid player could be found.
	 */
	public int determineStartingPlayer(HeartsGameState fromState){
		int startingPlayer = -1;
		Card<ClassicalSymbols, ClassicalSuits> startingCard = fromState.getStartingCard();
		for(Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : fromState.getMap().entrySet()){
			Card<ClassicalSymbols, ClassicalSuits> currentCard = entry.getKey();
			if(currentCard.equals(startingCard)){
				startingPlayer = entry.getValue().get(0).ordinal();
			}
		}
		return startingPlayer;
	}
	
	/**
	 * 
	 * This method checks if a given player only has cards of the suit hearts on his hand.
	 * 
	 * @param fromState HeartsGameState
	 * @param player Relevant player
	 * @return If player holds only hearts on his hand.
	 */
	public boolean isOnlyHeartsOnPlayershand(HeartsGameState fromState, int player){
		
		Map<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map = fromState.getMap();
		
		boolean onlyHearts = true;
		
		int cardCounter = 0;
		
		for(Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : map.entrySet()){
			// If given player holds this card..
			if(entry.getValue().get(0).equals(HeartsLocations.values()[player])){
				cardCounter++;
				// If card suit is not hearts: 
				if(!entry.getKey().getf2().equals(ClassicalSuits.HEARTS)){
					onlyHearts = false;
				}
			}
		}
		return onlyHearts && (cardCounter>0);
	}
	
	/**
	 * 
	 * This method usually takes the score array and returns the winner(s).
	 * 
	 * @param arrayToMinimize 
	 * @return The index(es) - or winners - of lowest number.
	 */
	public ArrayList<Double> multyArgMin(double[] arrayToMinimize){
		
		// First find minimum.
		double min = arrayToMinimize[0];
		for(int i=0; i<arrayToMinimize.length; i++){
			if(arrayToMinimize[i]<min){
				min = arrayToMinimize[i];
			}
		}
		
		// Find and add all indexes of the found minimum.
		ArrayList<Double> winners = new ArrayList<Double>();
		for(int i=0; i<arrayToMinimize.length; i++){
			if(arrayToMinimize[i] == min){
				winners.add(new Double(i));
			}
		}
				
		return winners;
	}
}
