package za.ac.sun.cs.ingenious.games.domineering;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.domineering.game.DomineeringAction;

public class DomineeringRandomEngine extends DomineeringEngine {
	
	public DomineeringRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		if (board == null) throw new RuntimeException(engineName()+": board was null when GenMoveMessage was received! (Perhaps no InitGameMessage was sent?)");
		// This engine just chooses a random, valid move
		List<Action> actions = logic.generateActions(board, playerID);
		if(!actions.isEmpty()){
			Random rand = new Random();
			return new PlayActionMessage((DomineeringAction)actions.get(rand.nextInt(actions.size())));
		} else
			return new PlayActionMessage(null);
	}

	@Override
	public String engineName() {
		return "DomineeringRandomEngine";		
	}
	
}
