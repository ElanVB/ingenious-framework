package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;

/**
 * This can be used to transform actions that were sent in by some player to the
 * corresponding moves that different players will observe.
 * For many games, every player always immediately observes all the actions taken by
 * every other player. For such cases, {@link PerfectInformationActionSensor} can be used.
 * 
 * Also see {@link Move} and {@link Action}.
 * 
 * @param <S> GameState of the game that this observer can be used for.
 */
public interface ActionSensor<S extends GameState> {
	
	/**
	 * @param originalAction The action that is being observed.
	 * @param fromGameState The state that the action is observed for.
	 * @param pointOfViewPlayerID The ID of the player that observes the given originalAction
	 * @return The move observed by the given player when the given action is applied to the given state
	 */
	public Move fromPointOfView(Action originalAction, S fromGameState, int pointOfViewPlayerID);

}
