package za.ac.sun.cs.ingenious.core.util;

import com.esotericsoftware.minlog.Log;
import com.google.common.eventbus.EventBus;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.zip.ZipFile;

public class GameSystem {
    private static final GameSystem instance = new GameSystem();

    private static final String RESOURCES_DIR = "";

    /* logginsearch paths to use during build */
    public static final String[] RESOURCES_BUILD_PATHS = {
            RESOURCES_DIR,
            "../src/main/resources/",
            "main/"
    };

    /* for optimization reasons */
    private Gson     gson;
    private EventBus eventBus;

    private GameSystem() {
        eventBus = new EventBus();
        gson = new Gson();

    }

    /**
     * @return	the single GameSystem instance containing all central information and an eventbus.
     */
    public static GameSystem getInstance() {
        return instance;
    }

    /**
     * @return the main system eventbus.
     */
    public EventBus getEventBus() {
        return eventBus;
    }

    public Gson getGson() {
        return gson;
    }

    public File getTopLevelPath() {
        try {
            return ClassUtils.getTopLevelDirectory(GameSystem.class);
        } catch (URISyntaxException e) {
            /* this shouldn't happen */
            throw new RuntimeException(e);
        }
    }

    public boolean isInsideJar() {
        File toplevel = getTopLevelPath();
        if (toplevel.isFile()) {
            try {
                ZipFile zf = new ZipFile(toplevel);
                zf.close();
                return true;
            } catch (IOException e) {
                Log.error(this.getClass().getName(),"Inside an unknown filetype container");
            }
        }
        return false;
    }

    public boolean isInsideDirectory() {
        File toplevel = getTopLevelPath();
        return toplevel.isDirectory();
    }

    private InputStream openStreamOrNull(File path) {
        try {
            return new FileInputStream(path);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * @param filename
     * @return	an InputStream corresponding to the resource (or null if it could not be located)
     */
    public InputStream openResource(String filename) {
        InputStream inputStream;

        if (!isInsideDirectory()) {
            if (!filename.startsWith("/")) {
                filename = "/" + filename;
            }
            File resourceFile = new File(filename);
            inputStream = getClass().getResourceAsStream(resourceFile.getPath());
        } else {
            File resourceFile = new File(filename);
            Log.info(resourceFile.getAbsoluteFile());
            inputStream = openStreamOrNull(resourceFile);
            /* resources could be located in the parent directories of the packages */
            int parents = 2;
            File base = getTopLevelPath();
            while (parents > 0 && inputStream == null) {
                base = new File(base, ".."); // go up one level
                resourceFile = base;
                resourceFile = new File(resourceFile, filename);
                inputStream = openStreamOrNull(resourceFile);
                parents--;
            }
        }
        return inputStream;
    }

    /* Tries all the build logginsearch paths to locate the file */
    public InputStream openResourceSeek(String filename) {
        InputStream is = null;
        int i = 0;
        while (is == null && i < GameSystem.RESOURCES_BUILD_PATHS.length) {
            /* FIXME this is due the way gradle compiles the projects */
            is = GameSystem.getInstance().openResource(
                    GameSystem.RESOURCES_BUILD_PATHS[i] + filename);
            i++;
        }
        return is;

    }

}
