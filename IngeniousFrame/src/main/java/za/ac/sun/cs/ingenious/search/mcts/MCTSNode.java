package za.ac.sun.cs.ingenious.search.mcts;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Type of search node to be used for plain MCTS. The {@link SearchNode} class already stores
 * all the necessary values for MCTS, so we don't have to add anything here.
 * The list of unexpanded moves for each node is initialized to the list of actions available
 * to the active player at that node.
 */
public class MCTSNode<S extends TurnBasedGameState> extends SearchNode<S, MCTSNode<S>> {
	
	public MCTSNode(S thisNodeState, GameLogic<S> logic, Move toThisNode, MCTSNode<S> parent) {
		super(thisNodeState, logic, toThisNode, parent,
				new ArrayList<Move>(logic.generateActions(thisNodeState, thisNodeState.nextMovePlayerID)));
	}

}
