package za.ac.sun.cs.ingenious.games.tictactoe.engines;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTEngine;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;
import za.ac.sun.cs.ingenious.search.minimax.MiniMax;

/**
 * This engine just chooses an action using Minimax search with alpha-beta pruning.
 */
public class TTTMinimaxEngine extends TTTEngine {

	public TTTMinimaxEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public String engineName() {
		return "TTTMinimaxEngine";	
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		TTTFinalEvaluator eval = new TTTFinalEvaluator();
		MiniMax<TurnBasedSquareBoard, TTTLogic, TTTFinalEvaluator> minimax = new MiniMax<TurnBasedSquareBoard, TTTLogic, TTTFinalEvaluator> (-1, eval, logic, this.playerID);
		return new PlayActionMessage(minimax.generateMove(currentState));
	}

}
