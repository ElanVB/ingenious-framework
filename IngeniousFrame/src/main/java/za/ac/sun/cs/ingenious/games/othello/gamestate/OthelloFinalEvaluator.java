package za.ac.sun.cs.ingenious.games.othello.gamestate;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;

/**
 * Class for evaluating terminal states for Bomberman
 */
public class OthelloFinalEvaluator implements GameFinalEvaluator<OthelloBoard> {

	private OthelloLogic logic = OthelloLogic.defaultOthelloLogic;

	/**
	 * Returns the scores for each player if the game is terminal. If the game
	 * is not terminal, null is returned.
	 */
	public double[] getScore(OthelloBoard forState) {
		return forState.getScore();
	}

	/**
	 * Returns the ID (number) of the winner or 2 for a draw, if the game is
	 * terminal; otherwise, returns -1.
	 *
	 * @return	The enum value representing the game state
	 */
	public short getWinner(OthelloBoard forState) {
		if (!logic.isTerminal(forState)) {
			return (short) GameFinalState.NOT_TERMINAL.value;
		}

		if (forState.blackScore > forState.whiteScore) {
			return (short) GameFinalState.BLACK_WIN.value;
		} else if (forState.blackScore < forState.whiteScore) {
			return (short) GameFinalState.WHITE_WIN.value;
		}

		return (short) GameFinalState.DRAW.value;
	}	
}
