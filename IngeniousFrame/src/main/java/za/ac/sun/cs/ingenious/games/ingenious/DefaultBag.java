package za.ac.sun.cs.ingenious.games.ingenious;
import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DefaultBag implements BagSequence {

	private ArrayList<Tile> bag;
	private final int numberOfColours;
	private final int capacity;
	
	public DefaultBag(int capacity,int numColours) {
		
		bag = new ArrayList<Tile>();
		this.capacity = capacity;
		this.numberOfColours=numColours;
		this.init();
		Collections.shuffle(this.bag);
	}
	
	@Override
	public int size(){
		return bag.size();
	}
	
	@Override
	public boolean isEmpty(){
		if(bag.size()==0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public Tile draw(){		
		Tile ret = bag.get(0);
		bag.remove(0);
		return ret;
	}
	
	@Override
	public Tile[] viewVisible(int lookAhead){
		Tile[] arr =  bag.toArray(new Tile[lookAhead]);
		return Arrays.copyOfRange(arr, 0, lookAhead);
	}

	private void init() {
		for (int k = 0; k < numberOfColours; k++) {
			addTwoColourTiles();
		}
		for (int k = 0; k < numberOfColours-1; k++) {
			addDoubleColourTiles();
		}		
	}
	
	private void addTwoColourTiles(){
		for (int i = 0; i < numberOfColours; i++) {
			for (int j = 0; j < numberOfColours; j++) {
				if (i >= j) {
					Tile tile = new Tile(i, j);
					bag.add(tile);
				}
			}
		}
	}
	
	private void addDoubleColourTiles(){
		for (int i = 0; i < numberOfColours; i++) {
			bag.add(new Tile(i, i));
		}
	}

	public static void main(String[] args) {
		//Constants.DEFAULT_NUM_TILES
		DefaultBag b = new DefaultBag(120,5);
		Tile[] arr = b.viewVisible(70);
		for(int i=0; i < 70;i++){
			Log.info(arr[i]);
		}
	}

}