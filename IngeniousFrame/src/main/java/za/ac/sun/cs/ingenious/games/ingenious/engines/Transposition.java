package za.ac.sun.cs.ingenious.games.ingenious.engines;


public class Transposition {
	long key;
	int [] bestMove;
	int value;
	int flag;
	int depth;
	public static final int EXACTVALUE = 0;
	public static final int LOWERBOUND = 1;
	public static final int UPPERBOUND = 2;
	
	public boolean equals( Object o){
		if( o instanceof Transposition){
			Transposition tranO = (Transposition)o;
			if(this.key == tranO.key){
				return true;
			}
		}
		return false;
	}
	public Transposition(long key, int[] bestMove,int value,int flag,int depth){
		this.key = key;
		this.bestMove = bestMove;
		this.value = value;
		this.flag = flag;
		this.depth = depth;
	}
}
