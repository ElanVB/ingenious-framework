package za.ac.sun.cs.ingenious.core.exception;

/**
 * Thrown by MatchSetting methods if a requested setting was not present.
 */
public class MissingSettingException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final String settingName;

	public MissingSettingException(String settingName) {
		super ("MatchSetting element with name " + settingName + " was not found in the JSON file");
		this.settingName = settingName;
	}

	public String getSettingName() {
		return settingName;
	}

}
