package za.ac.sun.cs.ingenious.games.bomberman.engines;

import com.esotericsoftware.minlog.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.bomberman.BMEngine;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.PlaceBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.TriggerBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;

public class BMEngineHuman extends BMEngine {

	public BMEngineHuman(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public String engineName() {
		return "HumanPlayer";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		BMGenActionMessage bmGenMove = (BMGenActionMessage)a;
		Action action = null;
	    Log.info("Player " +playerID+", please enter move (w, a, s, d to move, b to bomb or x to trigger, accept with enter): ");
	    Log.info("Your time limit in ms: "+a.getClock().getRemainingTimeMS());
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			while (bmGenMove.getClock().getRemainingTimeMS() > 10 && !in.ready()) {
				synchronized (this) {
					try {
						wait(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			String command = "";
			if (in.ready()) {
			    command = in.readLine();
			    if(command.length() > 1){
			    	command = command.substring(command.length() - 1);
			    }
			    Log.info("Player " +playerID+": You enter "+ command);
			} else {
			    Log.info("Player " +playerID+": You did not enter data.");
			}
		    switch(command){
		    	case "w": action = new CompassDirectionAction(bmGenMove.getPlayerID(), CompassDirection.N); break;
		    	case "s": action = new CompassDirectionAction(bmGenMove.getPlayerID(), CompassDirection.S); break;
		    	case "a": action = new CompassDirectionAction(bmGenMove.getPlayerID(), CompassDirection.W); break;
		    	case "d": action = new CompassDirectionAction(bmGenMove.getPlayerID(), CompassDirection.E); break;
		    	case "b": action = new PlaceBombAction(bmGenMove.getPlayerID()); break;
		    	case "x": action = new TriggerBombAction(bmGenMove.getPlayerID()); break;
		    }
			return new PlayActionMessage(action);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new PlayActionMessage(null);
	}
	
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		super.receiveGameTerminatedMessage(a); //Call super here to close connection to server.
	}
}
