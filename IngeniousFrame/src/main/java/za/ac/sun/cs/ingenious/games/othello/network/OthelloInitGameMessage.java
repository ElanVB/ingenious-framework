package za.ac.sun.cs.ingenious.games.othello.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * The message sent to Engines when a game has started. 
 * Holds the player's ID.
 *
 * @author Rudolf Stander
 *
 */
public class OthelloInitGameMessage extends InitGameMessage {

	/* The player's ID */
	private byte playerID;

	/**
	 * Constructs a new game initialization message with the player's ID.
	 *
	 * @param playerID	the players' ID
	 */
	public OthelloInitGameMessage(byte playerID)
	{
		this.playerID = playerID;
	}

	/**
	 * Returns the player's ID.
	 *
	 * @return	the player's ID
	 */
	public byte getPlayerID()
	{
		return playerID;
	}
}

