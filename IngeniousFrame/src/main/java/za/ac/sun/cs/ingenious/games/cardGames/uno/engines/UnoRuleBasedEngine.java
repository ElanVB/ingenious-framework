package za.ac.sun.cs.ingenious.games.cardGames.uno.engines;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.CardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.DrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.HiddenDrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoGameState;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoInitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.uno.game.UnoLocations;

/**
 * A rule based Uno engine.
 * 
 * Strategy: this engine tries to play the card which matches with most other cards on hand (normally beats random Uno players).
 * 
 * @author Joshua Wiebe
 */
public class UnoRuleBasedEngine extends Engine {

	/** A UnoGameState for internal representation. */
	UnoGameState state;
	
	/** A UnoGameLogic for internal representation. */
	UnoGameLogic logic;
	
	/** A rack to track whats on the current hand */
	CardRack<UnoSymbols, UnoSuits> hand;

	/**
	 * Instantiates a new Uno rule based engine.
	 *
	 * @param toServer the server connection
	 */
	public UnoRuleBasedEngine(EngineToServerConnection toServer) {
		super(toServer);
		logic = new UnoGameLogic();
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#engineName()
	 * 
	 * @return Returns it's name.
	 */
	@Override
	public String engineName() {
		return "UnoRuleBasedEngine";
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveInitAction(za.ac.sun.cs.ingenious.core.network.game.actions.InitGameAction)
	 * 
	 * Initialize the game state and update it with the given CardRack.
	 * 
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage m) {
		
		// Cast InitGameAction to UnoInitGameAction
		UnoInitGameMessage uiga = (UnoInitGameMessage) m;
		
		// Retrieve CardRack
		CardRack<UnoSymbols, UnoSuits> rack = uiga.getRack();
		
		// Save CardRack in local Rack
		hand = rack;
	
		// Initialize the game state with a Uno deck.
		CardDeck<UnoSymbols, UnoSuits> deck = new CardDeck<>(EnumSet.allOf(UnoSymbols.class), EnumSet.allOf(UnoSuits.class), 2);
		state = new UnoGameState(deck, uiga.getNumPlayers());

		// Update game state according to given rack.
		for (Card<UnoSymbols, UnoSuits> card : rack) {
			try {
				state.changeLocation(card, UnoLocations.DRAWPILE, UnoLocations.values()[super.playerID]);
			} catch (KeyNotFoundException e) {
				e.printStackTrace();
			} catch (LocationNotFoundException e) {
				e.printStackTrace();
			}
		}		
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receivePlayMove(za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 * 
	 * Update internal state according to received PlayMoveAction.
	 * 
	 * Cannot simply use makeMove() of UnoGameLogic because of imperfect information.
	 * 
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage m) {
		Move move = m.getMove();

		//similar to logic.makemove() but not the same. Cannot use logic.makemove() because of imperfect information (validMove() would not approve).
		
		// If DrawCardMove: Update from DRAWPILE to THIS! player.
		if (move instanceof DrawCardAction) {
			DrawCardAction<UnoLocations> dcMove = (DrawCardAction) move;
			try {
				// Update state
				state.changeLocation(dcMove.getCard(), UnoLocations.DRAWPILE, UnoLocations.values()[playerID]);
				state.incrementRackSize(playerID);
				
				//Update Hand
				hand.addCard(dcMove.getCard());
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		// If HiddenDrawCardMove: an other player drew a card. Update rackSize.
		else if(move instanceof HiddenDrawCardAction){
			HiddenDrawCardAction hdcMove = (HiddenDrawCardAction) move;
			state.incrementRackSize(hdcMove.getPlayerID());
		}
	
		// If PlayCardMove: Update either from DRAWPILE to new location or from this player to new location 
		// (distinction necessary because of imperfect information)
		else if (move instanceof PlayCardAction) {
			PlayCardAction<UnoLocations> pcMove = (PlayCardAction) move;
			state.changeTop(pcMove.getCard());
			state.decrementRackSize(pcMove.getPlayerID());
			
			//This player
			if (pcMove.getPlayerID() == this.getPlayerID()) {
				try {
					// Update state
					state.changeLocation(pcMove.getCard(), UnoLocations.values()[pcMove.getPlayerID()],
							(UnoLocations) pcMove.getNewLoc());
					
					// Update hand
					hand.removeCard(pcMove.getCard());
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			} 
			
			// Other player
			else {
				try {
					// Update state
					state.changeLocation(pcMove.getCard(), UnoLocations.DRAWPILE, (UnoLocations) pcMove.getNewLoc());
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		
		state.incrementRoundNR();

	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGenerateMove(za.ac.sun.cs.ingenious.core.network.game.actions.GenMoveAction)
	 * 
	 * Generate move.
	 * Strategy: this engine tries to play the card which matches with most other cards on hand (normally beats random Uno players).
	 * 
	 * @return best matching move
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage m) {
		List<Action> actions = (ArrayList<Action>) logic.generateActions(state, this.playerID);
		
		// If no possible moves
		if (actions.isEmpty()) {
			return null;
		}
		
		// Else: chose the move which has most matches with other cards on the hands.
		Action action = null;
		int mostMatches = 0, currentMatches = 0;
		for(Action currentAction : actions){
			currentMatches = countMatches(((CardAction)currentAction).getCard(), hand);
			if(currentMatches > mostMatches){
				action = currentAction;
				mostMatches = currentMatches;
			}
		}
		
		// Create PlayMoveAction containing the move and return it.
		PlayActionMessage pam = new PlayActionMessage(action);
		return pam;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGameTerminatedAction(za.ac.sun.cs.ingenious.core.network.game.actions.GameTerminatedAction)
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {

	}
	
	/**
	 * Given a card(type) this method counts the matches of a given list.
	 * 
	 * @param cardType The card for which the other 
	 * @param cardsToMatch
	 * 
	 * @return number of matches found
	 * 
	 */
	public int countMatches(Card<UnoSymbols, UnoSuits> cardType, CardRack<UnoSymbols, UnoSuits> cardsToMatch){
		
		// Counter for matches
		int cnt = 0;
		
		for(Card<UnoSymbols, UnoSuits> card : cardsToMatch){
			if(card.getf1().equals(cardType.getf1()) || card.getf2().equals(cardType.getf2())){
				cnt++;
			}
		}
		return cnt;
	}
	
	public void printMoves(List<Move> moves){
		CardRack<UnoSymbols, UnoSuits> rack = new CardRack<>();
		for(Move move :  moves){
			rack.addCard(((CardAction<UnoLocations>)move).getCard());
		}
		rack.printCards();
		Log.info();
	}
}
