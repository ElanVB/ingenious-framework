package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

/**
 * The class UnoInitGameAction.
 * 
 * @author Joshua Wiebe
 */
public class UnoInitGameMessage extends InitGameMessage{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The initial rack of a player. */
	private CardRack<UnoSymbols, UnoSuits> rack;
	
	/** The player ID for a specific player. */
	private int playerID;
	
	/** Number of players. */
	private int numPlayers;
	
	/**
	 * Instantiates a new unoInitGameAction.
	 *
	 * @param rack Initial rack
	 * @param playerID player ID
	 * @param numPlayers number of players
	 */
	public UnoInitGameMessage(CardRack<UnoSymbols, UnoSuits> rack, int playerID, int numPlayers){
		this.rack = rack;
		this.playerID = playerID;
		this.numPlayers = numPlayers;
	}
	
	/**
	 * Gets the rack.
	 *
	 * @return the rack
	 */
	public CardRack<UnoSymbols, UnoSuits> getRack(){
		return rack;
	}
	
	/**
	 * Gets number of players.
	 *
	 * @return number of players
	 */
	public int getNumPlayers(){
		return numPlayers;
	}
	
	/**
	 * Gets player ID.
	 *
	 * @return player ID
	 */
	public int getPlayerID(){
		return playerID;
	}
}
