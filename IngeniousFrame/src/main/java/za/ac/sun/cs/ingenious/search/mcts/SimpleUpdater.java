package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Standard implementation of the TreeUpdater. Just adds the given normalized results as rewards to each
 * node in the tree that was visited for that playout and increses the visit counts.
 * 
 * @author Michael Krause
 */
public class SimpleUpdater<N extends SearchNode<? extends GameState,N>> implements TreeUpdater<N> {

	@Override
	public void backupValue(N node, double[] normalizedResults) {
		N iterator = node;
		while (iterator != null) {
			iterator.addReward(normalizedResults);
			iterator.increaseVisits();
			iterator = iterator.getParent();
		}
	}
}
