package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Describes how the search tree is traversed and how the best node to expand is looked for.
 * 
 * @author steve
 */
public interface TreeDescender<S extends TurnBasedGameState, N extends SearchNode<S,N>> {
	/**
	 * Looking for the best move to play according to the current search state in root. This
	 * should be called after several rounds of the search have already been run.
	 * 
	 * @param root Node from which to act.
	 * @return Best action to play from root or null if there are no expanded children.
	 */
	public default Move bestPlayMove(N root) {
		// Returns the move leading to the child node with the highest visits count.
		// This isn't actually what most MCTS implementations do. 
		// Most implementations look for the highest search value, but in many cases the two will be equivalent.
		if (root.getExpandedChildren().isEmpty()) {
			return null;
		}
		
		int highestVisits = Integer.MIN_VALUE;
		N bestNode = null;
		for (N node : root.getExpandedChildren()) {			
			if (node.getVisits() > highestVisits) {
				highestVisits = node.getVisits();
				bestNode = node;
			}
		}
		return bestNode.getMove();
	}

	/**
	 * Looking for the best node to expand.
	 * 
	 * @param root Node from where to search.
	 * @param totalNofPlayouts Number of playouts that have been run before this call.
	 * @return The best node to expand according to the current search state that is reachable from root.
	 */
	public N descend(N root, int totalNofPlayouts);

	/**
	 * Choose a move to expand from parent, creates the appropriate child node and adds it to the tree.
	 * 
	 * @param parent Node to expand.
	 * @return Expanded child that has been added to parent or null if there is no move to expand.
	 */
	public N expand(N parent);
	
	/**
	 * @param fromNode Some node in a search tree.
	 * @return GameState to start a playout from for the given node.
	 */
	public S getStateForPlayout(N fromNode);
	
}