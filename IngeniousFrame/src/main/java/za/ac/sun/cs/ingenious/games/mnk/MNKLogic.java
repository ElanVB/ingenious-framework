package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.mnk.moves.MadeMarkMove;
import za.ac.sun.cs.ingenious.games.mnk.moves.OccupiedXYAction;
import za.ac.sun.cs.ingenious.games.mnk.moves.UncoveredMove;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

public class MNKLogic implements TurnBasedGameLogic<MNKState>, InformationSetDeterminizer<MNKState>, ActionSensor<MNKState> {

	private static final Random rand = new Random();

	private static final MNKFinalEvaluator eval = new MNKFinalEvaluator();

	@Override
	public boolean validMove(MNKState fromState, Move move) {
		if (move instanceof OccupiedXYAction) {
			return !fromState.isPerfectInformation();
		} else if (move instanceof XYAction) {
			XYAction a = (XYAction) move;
			if (!fromState.isPerfectInformation()) {
				return fromState.playerBoards.get(a.getPlayerID()).board[a.getX()][a.getY()] == 0;
			} else {
				return fromState.playerBoards.get(0).board[a.getX()][a.getY()] == 0;
			}
		} else if (move instanceof MadeMarkMove || move instanceof UncoveredMove){
			return true;			
		} else {
			return false;
		}
	}
	
	@Override
	public void nextTurn(MNKState fromState, Move move) {
		// First, set next player
		TurnBasedGameLogic.super.nextTurn(fromState, move);
		// Then, sync nextMovePlayerIDs for the contained player boards
		for (int i = 0; i < fromState.playerBoards.size(); i++) {
			fromState.playerBoards.get(i).nextMovePlayerID = fromState.nextMovePlayerID;
		}
	}

	@Override
	public boolean makeMove(MNKState fromState, Move move) {
		if (move instanceof UncoveredMove) {
			fromState.moveHistories.get(((UncoveredMove) move).getPlayerID()).add(move);
			return true;
		} else if (move instanceof MadeMarkMove) {
			fromState.moveHistories.get(((MadeMarkMove) move).getPlayerID()).add(move);
			nextTurn(fromState, move);
			return true;
		} else if (move instanceof OccupiedXYAction) {
			OccupiedXYAction xyMove = (OccupiedXYAction) move;
			fromState.moveHistories.get(xyMove.getUncoveringPlayer()).add(move);
			fromState.playerBoards.get(xyMove.getUncoveringPlayer()).board[xyMove.getX()][xyMove.getY()] = xyMove.getPlayerID() + 1;
			return true;
		} else if (move instanceof XYAction) {
			XYAction xyMove = (XYAction) move;
			fromState.moveHistories.get(xyMove.getPlayerID()).add(move);
			if (!fromState.isPerfectInformation()) {
				fromState.playerBoards.get(xyMove.getPlayerID()).board[xyMove.getX()][xyMove.getY()] = xyMove.getPlayerID() + 1;
			} else {
				fromState.playerBoards.get(0).board[xyMove.getX()][xyMove.getY()] = xyMove.getPlayerID() + 1;
			}
			nextTurn(fromState, move);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void undoMove(MNKState fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Action> generateActions(MNKState fromState, int forPlayerID) {
		if (forPlayerID < 0 || forPlayerID >= fromState.getNumPlayers()) {
			return new ArrayList<Action>();
		} else {
			TurnBased2DBoard b = null;
			if (!fromState.isPerfectInformation()) {
				b = fromState.playerBoards.get(forPlayerID);
			} else {
				b = fromState.playerBoards.get(0);
			}
			ArrayList<Action> al = new ArrayList<>();
			
			for(short h=0; h<b.getHeight();h++){
				for (short w = 0; w < b.getWidth(); w++) {
					if (b.board[h][w] == 0) {
						al.add(new XYAction(h, w, forPlayerID));
					}
				}
			}
			return al;
		}
	}

	@Override
	public boolean isTerminal(MNKState state) {
		if (eval.getWinner(state) != -2) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Move fromPointOfView(Action originalAction, MNKState fromGameState, int pointOfViewPlayerID) {
		if (!fromGameState.isPerfectInformation() && originalAction.getClass() == XYAction.class) {
			// For imperfect information play, resulting move might be different from submitted action
			XYAction xyAction = (XYAction) originalAction;
			int playerMakingMove = xyAction.getPlayerID();
			
			// Check if the spot to be played is already occupied
			for (int i = 0; i < fromGameState.getNumPlayers(); i++) {
				if (i == playerMakingMove) {
					continue;
				}
				TurnBased2DBoard opponentBoard = fromGameState.playerBoards.get(i);
				if (opponentBoard.board[xyAction.getX()][xyAction.getY()] == ((i) + 1)) { // If an opponent has already played here...
					// ... send an OccupiedXYAction if the player making the move or the referee is observing
					if (pointOfViewPlayerID == -1 || playerMakingMove == pointOfViewPlayerID) {
						return new OccupiedXYAction(xyAction.getX(), xyAction.getY(), i, playerMakingMove);
					} else { // ... and send a move if any other player is observing
						return new UncoveredMove(playerMakingMove);
					}
				}
			}

			// If the spot to be played is not yet occupied, send appropriate moves out
			if (pointOfViewPlayerID == -1 || playerMakingMove == pointOfViewPlayerID) {
				return originalAction;
			} else {
				return new MadeMarkMove(playerMakingMove);
			}
		}
		return originalAction;
	}

	@Override
	public MNKState determinizeUnknownInformation(MNKState forState, int forPlayerID) {
		if (forState.isPerfectInformation()) { // Nothing to determinize
			return forState.deepCopy();
		}
		
		// Now I repeatedly produce possible opponent move sequences until I arrive at a
		// determinized state that is not terminal
		MNKState detState = null;
		do {
			// Create the determinized state to return
			detState = forState.deepCopy(); // Copy move histories ... 
			// ... but not the boards:
			for (int i=0; i<detState.playerBoards.size();i++) {
				if (i!=forPlayerID) {
					detState.playerBoards.set(i, new TurnBased2DBoard(detState.playerBoards.get(i).getHeight(),
							detState.playerBoards.get(i).getWidth(), 0, detState.getNumPlayers()));
				}
			}

			// Some shortcuts
			final List<Move> myMoveHistory = forState.moveHistories.get(forPlayerID);

			// Firstly, consider all the opponent move that I already uncovered
			for (int i = 0; i < myMoveHistory.size(); i++) {
				// For all the opponent moves that I uncovered ...
				if (myMoveHistory.get(i) instanceof OccupiedXYAction) {
					// .. randomize the time at which my opponent made that move
					OccupiedXYAction m = (OccupiedXYAction) myMoveHistory.get(i);
					
					// Safety check: If I am the first player and uncovered an opponent move in
					// my first action, something went wrong
					if (i == 0 && forPlayerID == 0) {
						Log.error("MNKLogic.determinizeUnknownInformation",
								"Inconsistency: OccupiedXYAction at position==0 && myID==0");
					}

					List<Move> opponentMoveHistory = detState.moveHistories.get(m.getPlayerID());
					int maxMadeMarkMoveNumber = 0;
					for (int j = 0; j < i; j++) {
						if (myMoveHistory.get(j).getClass() == XYAction.class) {
							maxMadeMarkMoveNumber++;
						}
					}
					if (m.getPlayerID() < forPlayerID) {
						maxMadeMarkMoveNumber++;
					}
					
					int madeMarkMoveNumber = rand.nextInt(maxMadeMarkMoveNumber);
					int posInOppMoveHistory = -1;

					for (int j = 0; j < opponentMoveHistory.size(); j++) {
						if (opponentMoveHistory.get(j) instanceof MadeMarkMove || opponentMoveHistory.get(j).getClass() == XYAction.class) {
							if (madeMarkMoveNumber==0) {
								posInOppMoveHistory = j;
								break;
							} else {
								madeMarkMoveNumber--;
							}
						}
					}
					if (posInOppMoveHistory == -1) {
						Log.error("MNKLogic.determinizeUnknownInformation",
								"posInOppMoveHistory == -1");
						forState.printPretty();
						detState.printPretty();
					}
					
					opponentMoveHistory.set(posInOppMoveHistory, new XYAction(m.getX(), m.getY(), m.getPlayerID()));
				}
			}

			detState.moveHistories.get(forPlayerID).clear();
			detState.nextMovePlayerID = 0;
			// Now consider all the move that the opponent made that I don't know about
			// They may have uncovered my own marks or taken other free spots
			int[] moveHistoryIndices = new int[detState.moveHistories.size()];
			while (moveHistoryIndices[forPlayerID] < myMoveHistory.size()) {
				if (detState.nextMovePlayerID == forPlayerID) {
					makeMove(detState, myMoveHistory.get(moveHistoryIndices[forPlayerID]));
					moveHistoryIndices[forPlayerID]++;
					continue;
				}
				if (!(moveHistoryIndices[detState.nextMovePlayerID] < detState.moveHistories.get(detState.nextMovePlayerID).size())) {
					nextTurn(detState, null);
					continue;
				}
				
				int activePlayer = detState.nextMovePlayerID;
				List<Move> opponentMoveHistory = detState.moveHistories.get(activePlayer);
				if (opponentMoveHistory.get(moveHistoryIndices[activePlayer]) instanceof MadeMarkMove ||
						opponentMoveHistory.get(moveHistoryIndices[activePlayer]) instanceof UncoveredMove) {
					// ... make a random move
					List<Action> actions = generateActions(detState, activePlayer);
					for (int i = 0; i < actions.size();) {
						if ((opponentMoveHistory.get(moveHistoryIndices[activePlayer]) instanceof MadeMarkMove && 
								fromPointOfView(actions.get(i), detState, activePlayer) instanceof OccupiedXYAction) || 
							(opponentMoveHistory.get(moveHistoryIndices[activePlayer]) instanceof UncoveredMove && 
								fromPointOfView(actions.get(i), detState, activePlayer).getClass() == XYAction.class)) {
							actions.remove(i);
						} else {
							i++;
						}
					}

					if (!actions.isEmpty()) { // If any moves are possible...
						// ... pick a random one and ...
						Move povMove = fromPointOfView(actions.get(rand.nextInt(actions.size())), detState, activePlayer);
						opponentMoveHistory.set(moveHistoryIndices[activePlayer], povMove);
						// ... apply the opponent moves to the testState
						makeMove(detState, povMove);
						opponentMoveHistory.remove(opponentMoveHistory.size()-1);
					} else {
						Log.error("MNKLogic.determinizeUnknownInformation", "Logic error: opponent can't move");
						forState.printPretty();
						detState.printPretty();
					}
				}
				moveHistoryIndices[activePlayer]++;
			}
		} while (isTerminal(detState));

		detState.nextMovePlayerID = forState.nextMovePlayerID;

		return detState;
	}

	@Override
	public MNKState observeState(MNKState state, int forPlayerID) {
		MNKState newState = state.deepCopy();
		
		// If the perfect information game is played, all players observe the same state!
		if (!state.isPerfectInformation()) {
			// The player with id forPlayerID can only observe their own board
			for (int i = 0; i < newState.getNumPlayers(); i++) {
				if (i != forPlayerID) {
					newState.playerBoards.set(i,
							new TurnBased2DBoard(newState.playerBoards.get(i).getHeight(),
									newState.playerBoards.get(i).getWidth(), newState.playerBoards.get(i).nextMovePlayerID,
									newState.playerBoards.get(i).getNumPlayers()));
				}
			}

			// Furthermore, forPlayerID can only observe their own history completely, it sees the
			// other histories only partially (MadeMarkMove instead of XYAction, UncoveredMove instead of OccupiedXYAction)
			for (int i = 0; i < newState.moveHistories.size(); i++) {
				if (i != forPlayerID) {
					List<Move> hist = newState.moveHistories.get(i);
					for (int j = 0; j < hist.size(); j++) {
						if (hist.get(j).getClass() == XYAction.class) {
							hist.set(j, new MadeMarkMove(i));
						} else if (hist.get(j) instanceof OccupiedXYAction) {
							hist.set(j, new UncoveredMove(i));
						} 
					}
				}			
			}
		}

		return newState;
	}
}
