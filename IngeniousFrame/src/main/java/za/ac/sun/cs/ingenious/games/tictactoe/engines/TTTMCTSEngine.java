package za.ac.sun.cs.ingenious.games.tictactoe.engines;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTEngine;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.UCT;

/**
 * This engine just chooses an action using Monte Carlo Tree Search with random playouts
 */
public class TTTMCTSEngine extends TTTEngine {

	public TTTMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	@Override
	public String engineName() {
		return "TTTMCTSEngine";		
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		MCTSNode<TurnBasedSquareBoard> root = new MCTSNode<TurnBasedSquareBoard>(currentState, logic, null, null);
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<TurnBasedSquareBoard>(logic, new TTTFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), false),
				new MCTSDescender<TurnBasedSquareBoard>(logic, new UCT<>(), new PerfectInformationActionSensor<>()), 
				new SimpleUpdater<MCTSNode<TurnBasedSquareBoard>>(),
				Constants.TURN_LENGTH));
	}

}
